jQuery(document).ready(function($){
    var img_gal = $('.slideimg');
    $('#title_line').on('input', function() {
        $('.banner-title').html($('#title_line').val());
        check_slide_tag();
    });
    $('#tag_line').on('input', function() {
        $('.banner-text').html($('#tag_line').val());
        check_slide_tag();
    });

    $('#button_text').on('input', function() {
        $('.btn-slide').html($('#button_text').val());
        check_slide_tag();
    });


    if($('#sap_blue').attr('checked')) {
        $(".slide-tag").addClass('sap-blue');
    } else {
        $(".slide-tag").removeClass('sap-blue');
    }
    $('#sap_blue').click(function () {
        $(".slide-tag").toggleClass('sap-blue');
    });

    tagBoxLR = $("input:radio[name='_slide[box_side]']").val();
    if(tagBoxLR == 'right'){
        $(".slide-tag").removeClass('tagbox-left');
        $(".slide-tag").addClass('tagbox-right');
    }


    $("input:radio[name='_slide[box_side]']").click(function() {
        var value = $(this).val();
        if(value == 'right'){
            $(".slide-tag").removeClass('tagbox-left');
            $(".slide-tag").addClass('tagbox-right');
        }
        else{
            $(".slide-tag").removeClass('tagbox-right');
            $(".slide-tag").addClass('tagbox-left');
        }
    });

    check_slide_tag();
    check_upload_button();

    $( '.flkn-delete-slide' ).on( 'click', function(e)
    {
        e.preventDefault();
        if (confirm("Are you sure?")) {
            // your deletion code
            var img_gal = $('.slideimg');
            img_gal.attr("src", jquery_slide_upload.thumbnail_url);
            img_gal.addClass( 'default-image');
            $('#image_id').val('');
        }
        return false;
    });

    function check_upload_button(){
        $imageList = $( '#slide-preview' ),
        maxFileUploads = $imageList.data( 'max_file_uploads' ),
        uploaded = $imageList.children().length;
        $('#upload_logo_button').show();
        if(uploaded >= maxFileUploads)$('#upload_logo_button').hide();

        $('.flkn-delete-all-files').show();
        if(uploaded<=0)$('.flkn-delete-slide').hide();
    }

    function check_slide_tag(){
        $('.slide-tag').hide();
        titleText = $('#title_line').val();
        titleTag = $('#tag_line').val();
        if(titleText!='' || titleTag!='') $('.slide-tag').show();

    }

	var _custom_media = true,
    _orig_send_attachment = wp.media.editor.send.attachment;

	$('#upload_slide_button').click(function(e) {
        $uploadButton = $( this )
        $imageList = $( '#slide-preview' ),
        maxFileUploads = $imageList.data( 'max_file_uploads' );
        var slideworkflow = wp.media({
		title: 'Select slide image',
		// use multiple: false to disable multiple selection
		multiple: false,
		button: { 
			text: 'Add selected image'
		},
		library: {
			type: 'image'
		}
    });

var image_array = [];

function getAttachment(attachment) {
	attachment = attachment.toJSON();
	return {id:attachment.id,url:attachment.url,thumbnail:attachment.sizes.thumbnail};
}
 
function select() {

    var selection = slideworkflow.state().get( 'selection' ).toJSON(),
        uploaded = $imageList.children().length;
    ids = _.pluck( selection, 'id' );

    var selection = slideworkflow.state().get( 'selection' ).toJSON();
    selection = _.filter( selection, function( attachment )
    {
        return $imageList.children( 'li#img_id_' + attachment.id ).length == 0;

    } );

    selection.map( function( attachment ) {
        var thumb_url = attachment.sizes.hasOwnProperty('slide-image-thumb')? attachment.sizes.thumbnail.url : attachment.url ;
        $('.slideimg').attr("src", thumb_url);
        $('.slideimg').removeClass( 'default-image');
        $('#image_id').val(attachment.id);
    });
    check_upload_button();
}
 
function reset() {
	// called when dialog is closed by "close" button / "ESC" key
	// use the following line unbind the event
	// slideworkflow.off("select");
}
 
// bind event handlers
slideworkflow.on("select",select);
slideworkflow.on("escape",reset);
 
// open the dialog
slideworkflow.open();
	});
});