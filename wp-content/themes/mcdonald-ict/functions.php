<?php
/*
All the functions are in the PHP pages in the functions/ folder.
*/

require_once locate_template('/functions/cleanup.php');
require_once locate_template('/functions/setup.php');
require_once locate_template('/functions/enqueues.php');
require_once locate_template('/functions/navbar.php');
require_once locate_template('/functions/widgets.php');
require_once locate_template('/functions/search.php');
require_once locate_template('/functions/feedback.php');

add_action('after_setup_theme', 'true_load_theme_textdomain');

// Register WP alchemy
include_once 'metaboxes/setup.php';

// Custom Meta

include_once 'metaboxes/accordion_spec.php';
include_once 'metaboxes/norot_banner-spec.php';
include_once 'metaboxes/general_spec.php';
include_once 'metaboxes/grybx-spec.php';
//include_once 'metaboxes/gallery-spec.php';

include_once 'metaboxes/footer_spec.php';

//require_once( trailingslashit(get_stylesheet_directory()).'includes/post-types/cp_testimonials.php');
require_once( trailingslashit(get_stylesheet_directory()).'includes/post-types/cp_slide.php');
require_once( trailingslashit(get_stylesheet_directory()).'includes/post-types/cp_our-work.php');


//Load common shared Falkon functions
require_once( trailingslashit(get_stylesheet_directory()).'functions/functions-falkon.php');

require_once( trailingslashit(get_stylesheet_directory()).'includes/shortcodes.php');
//require_once( trailingslashit(get_stylesheet_directory()).'inc/widget.php');


function true_load_theme_textdomain(){
    load_theme_textdomain( 'bst', get_template_directory() . '/languages' );
}


// Blog searches - remove pages

function SearchFilter($query) {
	if ($query->is_search) {
		$query->set('post_type', 'post');
	}
	return $query;
}

add_filter('pre_get_posts','SearchFilter');



/* Modify the read more link on the_excerpt() */

function et_excerpt_length($length) {
	return 22;
}
add_filter('excerpt_length', 'et_excerpt_length');

/* Add a link  to the end of our excerpt contained in a div for styling purposes and to break to a new line on the page.*/

function et_excerpt_more($more) {
	global $post;
	return '';
}
add_filter('excerpt_more', 'et_excerpt_more');

// Pre styled text
function wpb_mce_buttons_2($buttons) {
	array_unshift($buttons, 'styleselect');
	return $buttons;
}
add_filter('mce_buttons_2', 'wpb_mce_buttons_2');
/*
* Callback function to filter the MCE settings
*/

function my_mce_before_init_insert_formats( $init_array ) {

// Define the style_formats array

	$style_formats = array(
		/*
		* Each array child is a format with it's own settings
		* Notice that each array has title, block, classes, and wrapper arguments
		* Title is the label which will be visible in Formats menu
		* Block defines whether it is a span, div, selector, or inline style
		* Classes allows you to define CSS classes
		* Wrapper whether or not to add a new block-level element around any selected elements
		*/
		array(
			'title' => 'Sub title',
			'block' => 'strong',
			'classes' => 'sub-title',
			'wrapper' => true,

		)
	);
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats );

	return $init_array;

}
// Attach callback to 'tiny_mce_before_init'
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );

//
////Load Page Custom Post type
//require_once( trailingslashit(get_stylesheet_directory()).'includes/post-types/cp_our-work.php');
//
