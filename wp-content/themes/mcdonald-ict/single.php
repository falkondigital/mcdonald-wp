<?php
/**
 * Single Post Template
 */
get_template_part('includes/header');

global $norotbanner_mb;
$nrbanner_meta = $norotbanner_mb->the_meta();

// Falkon Options
global $falkon_option;

?>
<section id="blgin-title">
  <div class="text-center">
	<h1><?php the_title()?></h1>
	  <?php echo do_shortcode("[addtoany buttons='facebook,twitter,google_plus']"); ?>
  </div>
</section>

<section id="blgin-banner">
  <div class="container">
		<?php
		if (isset( $nrbanner_meta['imgurl'])) {
			echo '<div class="banner-nrt">';
			echo '<img src="' . $nrbanner_meta['imgurl'] . '"/>';
			echo '<div class="banner-nrt-title hidden-xs">';
			echo '<span class="title">' . $nrbanner_meta['title_line'] . '</span>';
			echo '<span>' . $nrbanner_meta['ban_tag'] . '</span>';
			echo '</div>';
			echo '</div>';
		}
		else  {
			echo '<div class="no-img-upl">';
			echo '<img src="' . get_template_directory_uri() . '/images/blg-default.jpg" class="blog-banner-bl"/>';
			echo '<div class="banner-nrt-title hidden-xs">';
			echo '<span class="title">No Banner Image Uploaded</span>';
			echo '<span>On this pages settings, upload an image to appear in this area</span>';
			echo '</div>';
			echo '</div>';
		}
		?>
  </div><!-- /.container -->
</section>

<div class="container">
  <div class="row">

    <div class="col-xs-12 col-sm-12">
      <div id="content" role="main">
        <?php get_template_part('includes/loops/content', 'single'); ?>
      </div><!-- /#content -->
    </div>

  </div><!-- /.row -->
</div><!-- /.container -->

<div id="blg-bg">
	<div class="container">
		<div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 text-center">

			<?php
			echo '	<h3 class="related-title">Related news you might be interested in</h3>';
			$related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => 2, 'post__not_in' => array($post->ID) ) );
			if( $related ) foreach( $related as $post ) {
				setup_postdata($post); ?>
				<div class="col-xs-12 col-sm-6">
					<div class="col-xs-12 col-sm-12 blg-listing np">
						<article role="article" id="post_<?php the_ID()?>">

							<a href="<?php the_permalink()?>">
								<?php the_post_thumbnail('full', array('class'=>'blg-listing-img')); ?>
							</a>
							<div class="text-center">

								<?php
								echo '<h3>' . get_the_title() . '</h3>';
								echo '<span>' . get_the_date() . '</span>';
								echo  '<p>' . get_the_excerpt() . '</p>';
								?>


							</div>

						</article>
					</div>
				</div>
			<?php }
			wp_reset_postdata(); ?>

		</div>

	</div>
</div>

<?php get_template_part('includes/footer'); ?>
