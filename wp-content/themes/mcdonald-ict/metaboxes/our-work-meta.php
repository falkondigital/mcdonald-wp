<div class="my_meta_control">
    <?php //var_dump($mb->meta);?>
    <table class="form-table">
        <tbody>
        <tr>
            <?php
            $taxonomy = 'our-work-type';
            $your_taxonomy = get_taxonomy($taxonomy);
            $args = array(	'hide_empty' => 0,);
            $yourtaxonomies = get_terms($taxonomy,$args);
            ?>
            <th scope="row"><label for="<?php echo get_post_type().'_'.$taxonomy;?>"><?php _e($your_taxonomy->labels->singular_name,'mathsiscool');?>:</label></th>
            <td>

                <select name="<?php echo get_post_type().'_'.$taxonomy;?>" id="<?php echo get_post_type().'_'.$taxonomy;?>">
                    <?php
                    $names = wp_get_object_terms($post->ID, $taxonomy);
                    ?>
                    <option class="<?php echo $taxonomy;?>-option" value="" <?php if (!count($names)) echo "selected";?>><?php echo __('None', 'comsite') ?></option>
                    <?php
                    foreach ($yourtaxonomies as $yourtaxonomy) {
                        if ($post->post_type == 'our-work') //Add check because this function will also get called on post revision objects, and will result in double counting the newly added taxonomy relationship.
                            echo '<option class="'.$taxonomy.'-option" value="' . $yourtaxonomy->slug . '" '. ( !is_wp_error($names) && !empty($names) && !strcmp($yourtaxonomy->slug, $names[0]->slug)?'selected':'') .'>' . $yourtaxonomy->name . '</option>\n';
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <?php
            $taxonomy = 'client';
            $your_taxonomy = get_taxonomy($taxonomy);
            $args = array(	'hide_empty' => 0,);
            $yourtaxonomies = get_terms($taxonomy,$args);
            ?>
            <th scope="row"><label for="<?php echo get_post_type().'_'.$taxonomy;?>"><?php _e($your_taxonomy->labels->singular_name,'mathsiscool');?>:</label></th>
            <td>

                <select name="<?php echo get_post_type().'_'.$taxonomy;?>" id="<?php echo get_post_type().'_'.$taxonomy;?>">
                    <?php
                    $names = wp_get_object_terms($post->ID, $taxonomy);
                    ?>
                    <option class="<?php echo $taxonomy;?>-option" value="" <?php if (!count($names)) echo "selected";?>><?php echo __('None', 'comsite') ?></option>
                    <?php
                    foreach ($yourtaxonomies as $yourtaxonomy) {
                        if ($post->post_type == 'our-work') //Add check because this function will also get called on post revision objects, and will result in double counting the newly added taxonomy relationship.
                            echo '<option class="'.$taxonomy.'-option" value="' . $yourtaxonomy->slug . '" '. ( !is_wp_error($names) && !empty($names) && !strcmp($yourtaxonomy->slug, $names[0]->slug)?'selected':'') .'>' . $yourtaxonomy->name . '</option>\n';
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
	        <?php $mb->the_field('related_items'); ?>
	        <?php
	        $custom_post = 'our-work';
	        $custom_post_object = get_post_type_object( $custom_post );
	        $custom_post_name = $custom_post_object->labels->name;
	        ?>
	        <th scope="row">
		        <label for=""><?php _e('Related '.$custom_post_object->labels->name,'mathsiscool');?>:</label>
		        <a href="#" id="checkall-<?php echo $custom_post;?>" title="Check or uncheck all <?php echo $custom_post_name;?>">Check/Uncheck All</a>
		        <script type="text/javascript">
			        jQuery( document ).ready(function($) {
				        $("#checkall-<?php echo $custom_post;?>").click(function() {
					        var checkBoxes = $('#autochecklist-<?php echo $custom_post;?> .autocheck');
					        checkBoxes.prop("checked", !checkBoxes.prop("checked"));
					        return false;
				        });
				        return false;
			        });
		        </script>
	        </th>
	        <td>
		        <?php
		        $original_post = $post;
		        $args = array(
			        'post_type' => $custom_post,
			        'posts_per_page' => -1,
			        'post__not_in' => array( get_the_ID()),
			        'orderby'   => 'menu_order title'
		        );
		        $item_posts = get_posts( $args );

		        if ( count($item_posts)>0 ) :
			        echo '<div id="autochecklist-'.$custom_post.'"><ul class="">';
			        foreach( $item_posts as $post_rel ) :
				        setup_postdata($post_rel);
				        echo sprintf('<li><label for="%s">', $post_rel->ID );
				        echo sprintf('<input class="autocheck" type="checkbox" name="%s[]" value="%s" id="%2$s" %s/> %s</label></li>', $mb->get_the_name(), $post_rel->ID, $mb->get_the_checkbox_state($post_rel->ID),get_the_title($post_rel->ID) );
			        endforeach;
			        echo '</ul></div>';
		        else:
			        echo '<p>'.$custom_post_object->labels->not_found.'. <a href="'.admin_url('post-new.php?post_type='.$custom_post).'" target="_blank">Add one?</a></p>';
		        endif;
		        setup_postdata( $original_post);
		        ?>
	        </td>

        </tr>


        <label>Work Type</label>
        <?php $selected = ' selected="selected"'; ?>
        <?php $metabox->the_field('work-type'); ?>
        <select id="imageSelector" name="<?php $metabox->the_name(); ?>">
	        <option value=""></option>
	        <option value="wordpress"<?php if ($metabox->get_the_value() == 'wordpress') echo $selected; ?>>WordPress</option>
	        <option value="youtube"<?php if ($metabox->get_the_value() == 'youtube') echo $selected; ?>>YouTube</option>
	        <option value="magento"<?php if ($metabox->get_the_value() == 'magento') echo $selected; ?>>Magento</option>
	        <option value="shopify"<?php if ($metabox->get_the_value() == 'shopify') echo $selected; ?>>Shopify</option>
	        <option value="woocommerce"<?php if ($metabox->get_the_value() == 'woocommerce') echo $selected; ?>>WooCommerce</option>
        </select>


<!--        <select id="imageSelector" name="stat_icon" required>-->
<!--	        <option name="fire" value="falkon-lgo">Fire</option>-->
<!--	        <option name="traffic" value="traffic">Traffic</option>-->
<!--	        <option name="construction" value="construction">Construction</option>-->
<!--	        <option name="crash" value="crash">Crash</option>-->
<!--	        <option name="weather" value="weather">Weather</option>-->
<!--	        <option name="robbery" value="robbery">Robbery</option>-->
<!--	        <option name="deviation" value="deviation">Deviation</option>-->
<!--	        <option name="police" value="police">Police</option>-->
<!--        </select>-->


        <tr>
            <?php $mb->the_field('client_name'); ?>
            <th scope="row"><label for="">client_name</label></th>
            <td>
                <input type="text" readonly="readonly" aria-readonly="true" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>" />
                <br /><span class="description">Client name for sorting posts by post meta client name which matches taxonomies. Don't touch, it will populate on save to match Client above.</span>
            </td>
        </tr>
        </tbody>
    </table>
</div>