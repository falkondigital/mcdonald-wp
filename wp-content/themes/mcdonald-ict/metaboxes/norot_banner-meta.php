<div class="banner_meta_control">


        <?php global $wpalchemy_media_access; ?>


	                <p>
		                Upload an image for the banner of this page, suggested image size: 1900px by 440px
	                </p>
		                <?php $wpalchemy_media_access->setGroupName('img-n'. $mb->get_the_index())->setInsertButtonLabel('Insert Image'); ?>
                    <p>
                        <?php $mb->the_field('imgurl'); ?>
                        <?php echo $wpalchemy_media_access->getField(array('type'=>'hidden','name' => $mb->get_the_name(), 'value' => $mb->get_the_value(),'class'=>'upload-url')); ?>
                        <?php $mb->the_field('image_id'); ?>
                        <?php echo $wpalchemy_media_access->getField(array('type'=>'hidden','name' => $mb->get_the_name(), 'value' => $mb->get_the_value(),'class'=>'upload-id')); ?>
                        <?php echo $wpalchemy_media_access->getButton(array('label' => 'Upload Image')); ?>
                        <?php $mb->the_field('imgurl'); ?>
                        <?php
                        ?>
                        <img src="<?php echo (is_null($mb->get_the_value())?'':$mb->get_the_value());?>" class="slideimg default-image">
                    </p>


	<?php $mb->the_field('title_line'); ?>
	<label for="<?php $mb->the_name(); ?>">Banner title</label>
	<p>
		<input type="text" style="width:100%;" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" />
	</p>

	<?php $mb->the_field('ban_tag'); ?>
	<label for="<?php $mb->the_name(); ?>">Banner subtitle</label>
	<p>
		<textarea id="<?php $mb->the_name(); ?>" rows="2" name="<?php $mb->the_name(); ?>"><?php $mb->the_value(); ?></textarea>
	</p>

	<a href="#" class="dodelete button">Remove Document</a>

</div>