<?php global $grybx_meta;

global $grybx_meta;
$grybx = $grybx_meta->the_meta();
?>

<div class="my_meta_control">

       <label>This is an optionial text area</label>
       <span><em>If you do not want the grey text area to appear on the page, leave the following fields blank. <strong>Both fields</strong> need some content to be displayed.</em></span>
        <br>
        <p>
           <span>Enter A Title</span>
           <input type="text" name="<?php $mb->the_name('grey_title'); ?>" value="<?php $mb->the_value('grey_title'); ?>"/>
        </p>
        <br>
        <span>Enter Some Text</span>
        <?php $mb->the_field('grey_content'); ?>
        <?php
        $content = html_entity_decode($mb->get_the_value(),ENT_QUOTES, 'UTF-8');
        $id = sanitize_key($mb->get_the_name());//Needs to not have [] in the id, only letters and underscores
        $settings = array(
            'quicktags' => array(
                'buttons' => 'p,em,strong,link,ul,ol',
            ),
            /*'quicktags' => true,*/
            'tinymce' => true,
            'media_buttons'	=> false,
            'textarea_name'	=> $mb->get_the_name(),
            'textarea_rows'	=> 14,
            'teeny'			=> true,
        );

        wp_editor($content, $id, $settings);

        ?>






</div>

