<?php

$accordion_meta = new WPAlchemy_MetaBox(array
(
	'id' => '_accordion_meta',
	'title' => 'Service Accordion',
	'include_post_id' => 1537, // (Home Page)
	'context' => 'normal', // same as above, defaults to "normal"
	'priority' => 'high', // same as above, defaults to "high"
//	'save_action'	=>	'save_general_meta_data',
	'template' => get_stylesheet_directory() . '/metaboxes/accordion_meta.php'
));

/* eof */