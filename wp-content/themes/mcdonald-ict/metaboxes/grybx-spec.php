<?php

$grybx_meta = new WPAlchemy_MetaBox(array
(
	'id' => '_grybx_meta',
	'title' => 'Grey Text Area',
	'types' => array('page', 'post'), // added only for pages and to custom post type "events"
	'context' => 'normal', // same as above, defaults to "normal"
	'priority' => 'high', // same as above, defaults to "high"
    'exclude_post_id' => array('1537', '1649'),
	'template' => get_stylesheet_directory() . '/metaboxes/grybx-meta.php'
));

/* eof */