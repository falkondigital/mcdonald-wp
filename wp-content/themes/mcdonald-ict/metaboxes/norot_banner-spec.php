<?php
//add_action('do_meta_boxes', 'remove_feat_image_box');

//function remove_feat_image_box() {
//    remove_meta_box( 'postimagediv', 'page', 'side' );
//}



$norotbanner_mb = new WPAlchemy_MetaBox(array
(
	'id' => '_norot_banner_meta',
	'title' => 'Banner Information',
	'types' => array('page','post'), // added only for pages and to custom post type "events"
	'exclude_post_id' => array('1537','1649'),
	'context' => 'normal', // same as above, defaults to "normal"
	'priority' => 'high', // same as above, defaults to "high"
	'template' => get_stylesheet_directory() . '/metaboxes/norot_banner-meta.php',
	'mode' => WPALCHEMY_MODE_EXTRACT,
	'prefix' => '_norot_banner_meta_'
));