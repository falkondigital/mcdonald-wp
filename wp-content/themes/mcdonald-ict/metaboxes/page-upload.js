jQuery(document).ready(function($){
    var img_gal = $('.bannerimg');
    $('#banner_title').on('input', function() {
        $('.banner-title').html($('#banner_title').val());
        check_banner_tag();
    });
    $('#banner_tag').on('input', function() {
        $('.banner-text').html($('#banner_tag').val());
        check_banner_tag();
    });

    if($('#hide_fade').attr('checked')) {
        $(".banner-tag").addClass('clear-bg');
    } else {
        $(".banner-tag").removeClass('clear-bg');
    }
    $('#hide_fade').click(function () {
        $(".banner-tag").toggleClass('clear-bg');
    });


    check_banner_tag();
    check_upload_button();

    $( '.flkn-delete-banner' ).on( 'click', function(e)
    {
        e.preventDefault();
        if (confirm("Are you sure?")) {
            // your deletion code
            var img_gal = $('.bannerimg');
            img_gal.attr("src", '');
            $('#banner-preview').addClass('hidden');
            $('#table-hide-fade').addClass('hidden');
            $('#image_id').val('');
            check_upload_button();
        }
        return false;
    });

    function check_upload_button(){
        $imageList = $( '#banner-preview' ),
        maxFileUploads = $imageList.data( 'max_file_uploads' ),
        uploadedImageID = $('#image_id').val();
        $('#upload_banner_button').show();
        if(uploadedImageID!='')$('#upload_banner_button').hide();

        $('.flkn-delete-banner').show();
        if(uploadedImageID=='')$('.flkn-delete-banner').hide();
    }


    function check_banner_tag(){
        $('.banner-tag').hide();
        titleText = $('#banner_title').val();
        titleTag = $('#banner_tag').val();

        var img_height = $('#banner-preview').height();

        $('.banner-tag').height(img_height);

        var img_width = $('#banner-preview').width();

        var txtTitle = (24*img_width)/1170;
        var txtTag = (18*img_width)/1170;
        var widthTitle = (24*img_width)/1170;
        var marginTitle = (24*img_width)/1170;

        $('.banner-title').css({"fontSize": txtTitle});
        $('.banner-text').css("fontSize", txtTag);

        if(titleText!='' || titleTag!=''){
            $('.banner-tag').css('display','table');
            $('.banner-tag').removeClass('hidden');
        }
        else{
            $('.banner-tag').addClass('hidden');
        }
    }

	var _custom_media = true,
    _orig_send_attachment = wp.media.editor.send.attachment;

	$('#upload_banner_button').click(function(e) {
        $uploadButton = $( this )
        $imageList = $( '#banner-preview' ),
        maxFileUploads = $imageList.data( 'max_file_uploads' );
        var bannerworkflow = wp.media({
		title: 'Select banner image',
		// use multiple: false to disable multiple selection
		multiple: false,
		button: {
			text: 'Add selected image'
		},
		library: {
			type: 'image'
		}
    });

var image_array = [];

function getAttachment(attachment) {
	attachment = attachment.toJSON();
    //console.log(attachment);
	return {id:attachment.id,url:attachment.url,thumbnail:attachment.sizes.thumbnail};
}

function select() {
	// use this to unbind the event
	// bannerworkflow.off("select");
	// get all selected images (id/url attributes only)

    var selection = bannerworkflow.state().get( 'selection' ).toJSON(),
        uploaded = $imageList.children().length;
    ids = _.pluck( selection, 'id' );

    var selection = bannerworkflow.state().get( 'selection' ).toJSON();
    selection = _.filter( selection, function( attachment )
    {
        return $imageList.children( 'li#img_id_' + attachment.id ).length == 0;

    } );

    selection.map( function( attachment ) {
        var thumb_url = attachment.sizes.hasOwnProperty('banner-image-thumb')? attachment.sizes.thumbnail.url : attachment.url ;
        $('.bannerimg').attr("src", thumb_url);
        $('#banner-preview').removeClass('hidden');
        $('#table-hide-fade').removeClass('hidden');
        $('#image_id').val(attachment.id);
    });
    check_upload_button();
    check_banner_tag();
}

function reset() {
	// called when dialog is closed by "close" button / "ESC" key
	// use the following line unbind the event
	// bannerworkflow.off("select");
}

// bind event handlers
bannerworkflow.on("select",select);
bannerworkflow.on("escape",reset);

// open the dialog
bannerworkflow.open();
	});
});