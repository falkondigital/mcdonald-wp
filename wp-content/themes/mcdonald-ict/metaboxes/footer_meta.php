<?php
global $footercta_meta;
$footer_meta = $footercta_meta->the_meta();
?>

<div class="my_meta_control">

    <!-- START tickbox meta-->
    <label for="<?php $mb->the_name(); ?>">Hide Footer Call To Actions?</label>
    <?php $mb->the_field('hide_footer_cta'); ?>

    <label for="hide_footer_cta"><input type="checkbox" id="hide_footer_cta" name="<?php $mb->the_name(); ?>" value="1"<?php $mb->the_checkbox_state('1'); ?>/> TICK</label>
    <p class="description">Tick this box if you want to hide the CTA content at the bottom of the page.</p>
    <!--END tickbox meta-->

    <?php
    if($footer_meta['hide_footer_cta']=='1') {
        echo '<p>This section is currently <strong>hidden</strong> - untick the box below to show it</p>';
    } else {
        echo '<p>This section is currently <strong>showing</strong> <br> You will see a blue area with a button at the bottom of this page</p>';
        ?>
<!---->
<!--    <p>-->
<!--        <span>Enter Tagline</span>-->
<!--        <em>Default value is Book a free, no obligation consulation now Book Now</em>-->
<!--        <input type="text" name="--><?php //$mb->the_name('ctatag'); ?><!--" value="--><?php //$mb->the_value('ctatag'); ?><!--"/>-->
<!--    </p>-->
<!--    <p>-->
<!--       <span>Enter Some Button Text</span>-->
<!--        <em>Default value is Book Now</em>-->
<!--       <input type="text" name="--><?php //$mb->the_name('ctabtn'); ?><!--" value="--><?php //$mb->the_value('ctabtn'); ?><!--"/>-->
<!--    </p>-->
    <?php
    }
    ?>

</div>

