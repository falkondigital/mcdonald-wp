<?php global $accordion_meta; ?>

<div class="my_meta_control">

	<!-- Tab ONE -->
    <tr><label for="button_text">Accordion Tab One</label></tr><br>
	<tr>
		<?php $mb->the_field('font_awesome_ico'); ?>
		<th scope="row"><strong>Font Awesome Icon</strong></th>
		<td>
			<input type="text"
			       class="widefat"
			       name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" id=""
			       maxlength="20" />
			<p class="description">Enter the font awesome code here, for example: fa-user</p>
		</td>
	</tr>
	<tr>
		<?php $mb->the_field('tab_title'); ?>
		<th scope="row"><strong>Tab Title</strong></th>
		<td>
			<input type="text"
			       class="widefat"
			       name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" id=""
			       maxlength="100" />
			<p class="description">Text for the button on the slide. Leave blank to not display the button.</p>
		</td>
	</tr>
	<tr>
		<th scope="row"><strong>Tab Content</strong></th>
		<td>
			<?php $mb->the_field('tab_desc'); ?>
			<?php
			$content = html_entity_decode($mb->get_the_value(),ENT_QUOTES, 'UTF-8');
			$id = sanitize_key($mb->get_the_name());//Needs to not have [] in the id, only letters and underscores
			$settings = array(
				'quicktags' => array(
					'buttons' => 'p,em,strong,link,ul,ol',
				),
				/*'quicktags' => true,*/
				'tinymce' => true,
				'media_buttons'	=> false,
				'textarea_name'	=> $mb->get_the_name(),
				'textarea_rows'	=> 8,
				'teeny'			=> true,
			);

			wp_editor($content, $id, $settings);

			?>
		</td>
	</tr>

    <!-- Tab TWO -->
	<tr><label for="button_text">Accordion Tab Two</label></tr><br>
	<tr>
		<?php $mb->the_field('font_awesome_ico2'); ?>
		<th scope="row"><strong>Font Awesome Icon</strong></th>
		<td>
			<input type="text"
			       class="widefat"
			       name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" id=""
			       maxlength="20" />
			<p class="description">Enter the font awesome code here, for example: fa-user</p>
		</td>
	</tr>
	<tr>
		<?php $mb->the_field('tab_title2'); ?>
		<th scope="row"><strong>Tab Title</strong></th>
		<td>
			<input type="text"
			       class="widefat"
			       name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" id=""
			       maxlength="100" />
			<p class="description">Text for the button on the slide. Leave blank to not display the button.</p>
		</td>
	</tr>
	<tr>
		<th scope="row"><strong>Tab Content</strong></th>
		<td>
			<?php $mb->the_field('tab_desc2'); ?>
			<?php
			$content = html_entity_decode($mb->get_the_value(),ENT_QUOTES, 'UTF-8');
			$id = sanitize_key($mb->get_the_name());//Needs to not have [] in the id, only letters and underscores
			$settings = array(
				'quicktags' => array(
					'buttons' => 'p,em,strong,link,ul,ol',
				),
				/*'quicktags' => true,*/
				'tinymce' => true,
				'media_buttons'	=> false,
				'textarea_name'	=> $mb->get_the_name(),
				'textarea_rows'	=> 8,
				'teeny'			=> true,
			);

			wp_editor($content, $id, $settings);

			?>
		</td>
	</tr>

	<!-- Tab THREE -->
	<tr><label for="button_text">Accordion Tab Three</label></tr><br>
	<tr>
		<?php $mb->the_field('font_awesome_ico3'); ?>
		<th scope="row"><strong>Font Awesome Icon</strong></th>
		<td>
			<input type="text"
			       class="widefat"
			       name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" id=""
			       maxlength="20" />
			<p class="description">Enter the font awesome code here, for example: fa-user</p>
		</td>
	</tr>
	<tr>
		<?php $mb->the_field('tab_title3'); ?>
		<th scope="row"><strong>Tab Title</strong></th>
		<td>
			<input type="text"
			       class="widefat"
			       name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" id=""
			       maxlength="100" />
			<p class="description">Text for the button on the slide. Leave blank to not display the button.</p>
		</td>
	</tr>
	<tr>
		<th scope="row"><strong>Tab Content</strong></th>
		<td>
			<?php $mb->the_field('tab_desc3'); ?>
			<?php
			$content = html_entity_decode($mb->get_the_value(),ENT_QUOTES, 'UTF-8');
			$id = sanitize_key($mb->get_the_name());//Needs to not have [] in the id, only letters and underscores
			$settings = array(
				'quicktags' => array(
					'buttons' => 'p,em,strong,link,ul,ol',
				),
				/*'quicktags' => true,*/
				'tinymce' => true,
				'media_buttons'	=> false,
				'textarea_name'	=> $mb->get_the_name(),
				'textarea_rows'	=> 8,
				'teeny'			=> true,
			);

			wp_editor($content, $id, $settings);

			?>
		</td>
	</tr>

</div>