<?php

function bst_search_form( $form ) {
	$form = '
	<form>
  <div class="input-group">
    <input class="form-control mob-form" type="text" value="' . get_search_query() . '" placeholder="' . esc_attr__('Search', 'bst') . '..." name="s" id="s">
    <div class="input-group-btn">
      <button type="submit" id="searchsubmit" value="'. esc_attr__('Search', 'bst') .'" class="btn btn-default" style="height:34px;">
        <i class="glyphicon glyphicon-search"></i>
      </button>
    </div>
  </div>
</form>
    ';
	return $form;
}
add_filter( 'get_search_form', 'bst_search_form' );

//
//       OLD FORM
//<form class="form-inline" role="search" method="get" id="searchform" action="' . home_url('/') . '" >

//	<input class="form-control" type="text" value="' . get_search_query() . '" placeholder="' . esc_attr__('Search', 'bst') . '..." name="s" id="s" />
//	<button type="submit" id="searchsubmit" value="'. esc_attr__('Search', 'bst') .'" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
//
//</form>