<?php

function bst_enqueues() {

	/* Styles */

//	wp_register_style('bootstrap-css', get_template_directory_uri() . '/css/bootstrap.min.css', false, '3.3.4', null);
//	wp_enqueue_style('bootstrap-css');

//  	wp_register_style('bst-css', get_template_directory_uri() . '/css/bst.css', false, null);
//	wp_enqueue_style('bst-css');

	wp_register_style('theme-css', get_template_directory_uri() . '/css/style.css', false, null);
	wp_enqueue_style('theme-css');

	// Animate CSS for transitions
	wp_register_style('animate-css', get_template_directory_uri() . '/css/components/animate.css', false, null);
	wp_enqueue_style('animate-css');

	wp_register_style('owl-css', get_template_directory_uri() . '/includes/owl-carousel/owl.carousel.css', false, null);
	wp_enqueue_style('owl-css');

	wp_register_style('owl-transitions', get_template_directory_uri() . '/includes/owl-carousel/owl.transitions.css', false, null);
	wp_enqueue_style('owl-transitions');

	wp_register_style('owl-theme', get_template_directory_uri() . '/includes/owl-carousel/owl.theme.css', false, null);
	wp_enqueue_style('owl-theme');

	//Include theme support for Font Awesome icon pack
	wp_enqueue_style( 'font-awesome',"//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css",array('theme-css'),'4.7.0');

	//Include theme support for all Open Sans font from Google APIS
	$query_args = array(
		'family' => 'Open+Sans:300,300i,400,400i,600,600i,700,700i',
		'subset'	=>	'latin-ext',
	);
	wp_enqueue_style( 'google_fonts_all', add_query_arg( $query_args, "//fonts.googleapis.com/css" ), array(), null );


	/* Scripts */

	wp_enqueue_script( 'jquery' );
	/* Note: this above uses WordPress's onboard jQuery. You can enqueue other pre-registered scripts from WordPress too. See:
	https://developer.wordpress.org/reference/functions/wp_enqueue_script/#Default_Scripts_Included_and_Registered_by_WordPress */

  	wp_register_script('modernizr', get_template_directory_uri() . '/js/modernizr-2.8.3.min.js', false, null, true);
	wp_enqueue_script('modernizr');

  	wp_register_script('bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', false, null, true);
	wp_enqueue_script('bootstrap-js');

	wp_register_script('bst-js', get_template_directory_uri() . '/js/bst.js', false, null, true);
	wp_enqueue_script('bst-js');

	wp_register_script('owl-js', get_template_directory_uri() . '/includes/owl-carousel/owl.carousel.js', false, null, true);
	wp_enqueue_script('owl-js');



	if (is_singular() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}
}
add_action('wp_enqueue_scripts', 'bst_enqueues', 100);
