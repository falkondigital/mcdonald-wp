<?php

//Theme Options Settings
if(is_admin() and ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX )) require_once( trailingslashit(get_stylesheet_directory()).'admin/options/falkon-theme-settings-advanced.php');

/**
 * Collects falkon theme options
 *
 * @return array
 */
function falkon_get_global_options(){
    $falkon_option = array();
    // collect option names as declared in wptuts_get_settings()
    $falkon_option_names = array (
        'falkon_options_general',
        'falkon_options_homepage',
        'falkon_options_hp_blocks',
        'falkon_options_contacts',
        'falkon_options_login_section',
        'falkon_options_page_ids',
	    'falkon_options_our_work',
        'falkon_options_social_media',
        'falkon_options_gocardless',
        'falkon_options_smtp_email',
        // 'wptuts_options_two_select',
        // 'wptuts_options_two_checkboxes'
    );
    // loop for get_option
    foreach ($falkon_option_names as $falkon_option_name) {
        if (get_option($falkon_option_name)!= FALSE) {
            $option     = get_option($falkon_option_name);
            // now merge in main $wptuts_option array!
            $falkon_option = array_merge($falkon_option, $option);
        }
    }
    return $falkon_option;
}
/**
 * Call the function and collect in variable
 *
 * Should be used in template files like this:
 * <?php echo $falkon_option['falkon_txt_input']; ?>
 *
 * Note: Should you notice that the variable ($falkon_option) is empty when used in certain templates such as header.php, sidebar.php and footer.php
 * you will need to call the function (copy the line below and paste it) at the top of those documents (within php tags)!
 */
global $falkon_option;
$falkon_option = falkon_get_global_options();


add_filter( 'login_headerurl', 'namespace_login_headerurl' );

/**
 * Replaces the login header logo URL
 *
 * @param $url
 */
function namespace_login_headerurl( $url ) {
    $url = home_url( '/' );
    return $url;
}
add_filter( 'login_headertitle', 'namespace_login_headertitle' );
/**
 * Replaces the login header logo title
 *
 * @param $title
 */
function namespace_login_headertitle( $title ) {
    $title = get_bloginfo( 'name' );
    return $title;
}



function falkon_footer_admin () {
    echo 'Powered by <a href="http://www.wordpress.org" target="_blank" rel="nofollow">WordPress</a> ('.get_bloginfo('version').') | Designed by <a href="http://www.falkondigital.com/?utm_source='.sanitize_title_with_dashes(get_bloginfo('name')).'" target="_blank">Falkon Digital</a> | email <a href="mailto:info@falkondigital.com" target="_blank" rel="nofollow">info@falkondigital.com</a> for support.</p>';
}
add_filter('admin_footer_text', 'falkon_footer_admin');

/*Custom log function to log messages to log file*/
if(!function_exists('_log')){
    function _log( $message ) {
        if( WP_DEBUG === true ){
            if( is_array( $message ) || is_object( $message ) ){
                error_log( print_r( $message, true ) );
            } else {
                error_log( $message );
            }
        }
    }
}

function hide_update_notice_to_all_but_admin_users()
{
    if (!current_user_can('update_core')) {
        remove_action( 'admin_notices', 'update_nag', 3 );
    }
}
add_action( 'admin_head', 'hide_update_notice_to_all_but_admin_users', 1 );

/**
 * Return single array value as not array
 *
 * @param $a
 * @return mixed
 */
function dba_mapusers( $a ){ return $a[0]; }




function remove_wp_logo( $wp_admin_bar ) {
    global $wp_admin_bar;
//    var_dump($wp_admin_bar);
    $wp_admin_bar->remove_node( 'wp-logo' );
    $wp_admin_bar->remove_node( 'dashboard');
    $wp_admin_bar->remove_node( 'appearance' );
    $wp_admin_bar->remove_node( 'themes' );
    $wp_admin_bar->remove_node( 'customize' );
    $wp_admin_bar->remove_node( 'menus' );
}
add_action( 'admin_bar_menu', 'remove_wp_logo', 999 );

/**
Re-use some existing WordPress functions so you don't have to write a bunch of raw PHP to check for SSL, port numbers, etc

Place in your functions.php (or re-use in a plugin)

If you absolutely don't need or want any query string, use home_url(add_query_arg(array(),$wp->request));

Hat tip to:
+ http://kovshenin.com/2012/current-url-in-wordpress/
+ http://stephenharris.info/how-to-get-the-current-url-in-wordpress/
 */

/**
 * Build the entire current page URL (incl query strings) and output it
 * Useful for social media plugins and other times you need the full page URL
 * Also can be used outside The Loop, unlike the_permalink
 *
 * @returns the URL in PHP (so echo it if it must be output in the template)
 * Also see the_current_page_url() syntax that echoes it
 */
if ( ! function_exists( 'get_current_page_url' ) ) {
    function get_current_page_url() {
        global $wp;
        return add_query_arg( $_SERVER['QUERY_STRING'], '', home_url( $wp->request ) );
    }
}

/*
* Shorthand for echo get_current_page_url();
* @returns echo'd string
*/
if ( ! function_exists( 'the_current_page_url' ) ) {
    function the_current_page_url() {
        echo get_current_page_url();
    }
}



/*
 * Strips all non digits and formats price to 123.45 interger
 */
function format_price($number = NULL){
    if($number == NULL) return 0.00;
    $pattern = '/[^0-9.]*/';
    $number = preg_replace($pattern,'', $number);
    //$number = ($number==0)? 0.99: $number;	//fix for multiple ...
    $number = number_format((float)$number, 2, '.', '');
    return (float)$number;

}


/**
 * Checks if a particular user has a role.
 * Returns true if a match was found.
 *
 * @param string $role Role name.
 * @param int $user_id (Optional) The ID of a user. Defaults to the current user.
 * @return bool
 */
function appthemes_check_user_role( $role, $user_id = null ) {

    if ( is_numeric( $user_id ) )
        $user = get_userdata( $user_id );
    else
        $user = wp_get_current_user();

    if ( empty( $user ) )
        return false;

    return in_array( $role, (array) $user->roles );
}


/* Postcode formatter */
function formatBritishPostcode($postcode) {

    //--------------------------------------------------
    // Clean up the user input

    $postcode = strtoupper($postcode);
    $postcode = preg_replace('/[^A-Z0-9]/', '', $postcode);
    $postcode = preg_replace('/([A-Z0-9]{3})$/', ' \1', $postcode);
    $postcode = trim($postcode);

    //--------------------------------------------------
    // Check that the submitted value is a valid
    // British postcode: AN NAA | ANN NAA | AAN NAA |
    // AANN NAA | ANA NAA | AANA NAA

    if (preg_match('/^[a-z](\d[a-z\d]?|[a-z]\d[a-z\d]?) \d[a-z]{2}$/i', $postcode)) {
        return $postcode;
    } else {
        return false;
    }
}


function falkon_get_wpadmin_localtime_from_timestamp($timestamp = 0,$output_format = null){
    if((int)$timestamp==0) return false;
    $output_format = is_null($output_format)?get_option('date_format') . ' - '. get_option('time_format'):$output_format;
    return get_date_from_gmt( date( 'Y-m-d H:i:s', $timestamp ), $output_format );
    //get_option('date_format') . ' - '. get_option('time_format')
}


function get_relative_permalink( $url ) {
    return str_replace( home_url(), "", $url );
}


function convert_number_to_words($number) {

    $hyphen      = '-';
    $conjunction = ' and ';
    $separator   = ', ';
    $negative    = 'negative ';
    $decimal     = ' point ';
    $dictionary  = array(
        0                   => 'zero',
        1                   => 'one',
        2                   => 'two',
        3                   => 'three',
        4                   => 'four',
        5                   => 'five',
        6                   => 'six',
        7                   => 'seven',
        8                   => 'eight',
        9                   => 'nine',
        10                  => 'ten',
        11                  => 'eleven',
        12                  => 'twelve',
        13                  => 'thirteen',
        14                  => 'fourteen',
        15                  => 'fifteen',
        16                  => 'sixteen',
        17                  => 'seventeen',
        18                  => 'eighteen',
        19                  => 'nineteen',
        20                  => 'twenty',
        30                  => 'thirty',
        40                  => 'fourty',
        50                  => 'fifty',
        60                  => 'sixty',
        70                  => 'seventy',
        80                  => 'eighty',
        90                  => 'ninety',
        100                 => 'hundred',
        1000                => 'thousand',
        1000000             => 'million',
        1000000000          => 'billion',
        1000000000000       => 'trillion',
        1000000000000000    => 'quadrillion',
        1000000000000000000 => 'quintillion'
    );

    if (!is_numeric($number)) {
        return false;
    }

    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
        // overflow
        trigger_error(
            'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
            E_USER_WARNING
        );
        return false;
    }

    if ($number < 0) {
        return $negative . convert_number_to_words(abs($number));
    }

    $string = $fraction = null;

    if (strpos($number, '.') !== false) {
        list($number, $fraction) = explode('.', $number);
    }

    switch (true) {
        case $number < 21:
            $string = $dictionary[$number];
            break;
        case $number < 100:
            $tens   = ((int) ($number / 10)) * 10;
            $units  = $number % 10;
            $string = $dictionary[$tens];
            if ($units) {
                $string .= $hyphen . $dictionary[$units];
            }
            break;
        case $number < 1000:
            $hundreds  = $number / 100;
            $remainder = $number % 100;
            $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
            if ($remainder) {
                $string .= $conjunction . convert_number_to_words($remainder);
            }
            break;
        default:
            $baseUnit = pow(1000, floor(log($number, 1000)));
            $numBaseUnits = (int) ($number / $baseUnit);
            $remainder = $number % $baseUnit;
            $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
            if ($remainder) {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= convert_number_to_words($remainder);
            }
            break;
    }

    if (null !== $fraction && is_numeric($fraction)) {
        $string .= $decimal;
        $words = array();
        foreach (str_split((string) $fraction) as $number) {
            $words[] = $dictionary[$number];
        }
        $string .= implode(' ', $words);
    }

    return $string;
}


function falkon_encode($string,$key) {
    $key = sha1($key);
    $strLen = strlen($string);
    $keyLen = strlen($key);
    $j = 0;
    $hash = '';
    for ($i = 0; $i < $strLen; $i++) {
        $ordStr = ord(substr($string,$i,1));
        if ($j == $keyLen) { $j = 0; }
        $ordKey = ord(substr($key,$j,1));
        $j++;
        $hash .= strrev(base_convert(dechex($ordStr + $ordKey),16,36));
    }
    return $hash;
}
function falkon_decode($string,$key) {
    $key = sha1($key);
    $strLen = strlen($string);
    $keyLen = strlen($key);
    for ($i = 0; $i < $strLen; $i+=2) {
        $ordStr = hexdec(base_convert(strrev(substr($string,$i,2)),36,16));
        if ($j == $keyLen) { $j = 0; }
        $ordKey = ord(substr($key,$j,1));
        $j++;
        $hash .= chr($ordStr - $ordKey);
    }
    return $hash;
}

function my_mce_buttons_2($buttons) {
    /**
     * Add in a core button that's disabled by default
     */
    $buttons[] = 'superscript';
    $buttons[] = 'subscript';

    return $buttons;
}
add_filter('mce_buttons_2', 'my_mce_buttons_2');

// Enable font size & font family selects in the editor
if ( ! function_exists( 'wpex_mce_buttons' ) ) {
    function wpex_mce_buttons( $buttons ) {
//        array_unshift( $buttons, 'fontselect' ); // Add Font Select
        array_unshift( $buttons, 'fontsizeselect' ); // Add Font Size Select
        return $buttons;
    }
}
add_filter( 'mce_buttons_2', 'wpex_mce_buttons' );


function mic_get_address($school_id=0,$one_line=false){
    if($school_id==0)
        return 'No address details';

    $school_address = '';
    $breaker = $one_line?', ':'<br>';

    global $school_mb;
    $school_meta = $school_mb->the_meta($school_id);
    $school_address .= '<strong>'.get_the_title($school_id).'</strong>'.$breaker;
    if($school_meta['address_1']) $school_address .= $school_meta['address_1'].$breaker;
    if($school_meta['address_2']) $school_address .= $school_meta['address_2'].$breaker;
    if($school_meta['towncity']) $school_address .= $school_meta['towncity'].$breaker;
    if($school_meta['county']) $school_address .= $school_meta['county'].$breaker;
    if($school_meta['postcode']) $school_address .= $school_meta['postcode'];

    return $school_address;
}


/*
 * WordPress Breadcrumbs
 * author: Dimox
 * version: 2015.05.21
*/
function dimox_breadcrumbs() {

    /* === OPTIONS === */
    $text['home']     = 'Home'; // text for the 'Home' link
    $text['blog']     = get_the_title( get_option( 'page_for_posts' ) );
    $text['category'] = 'Archive by Category "%s"'; // text for a category page
    $text['search']   = 'Search Results for "%s" Query'; // text for a search results page
    $text['tag']      = 'Posts Tagged "%s"'; // text for a tag page
    $text['author']   = 'Articles Posted by %s'; // text for an author page
    $text['404']      = 'Error 404'; // text for the 404 page
    $text['page']     = 'Page %s'; // text 'Page N'
    $text['cpage']    = 'Comment Page %s'; // text 'Comment Page N'

    $delimiter      = '<i class="fa fa-angle-right"></i>'; // delimiter between crumbs
    $delim_before   = '<span class="divider">'; // tag before delimiter
    $delim_after    = '</span>'; // tag after delimiter
    $show_home_link = 1; // 1 - show the 'Home' link, 0 - don't show
    $show_on_home   = 1; // 1 - show breadcrumbs on the homepage, 0 - don't show
    $show_current   = 1; // 1 - show current page title, 0 - don't show
    $show_title     = 1; // 1 - show the title for the links, 0 - don't show
    $before         = '<span class="current">'; // tag before the current crumb
    $after          = '</span>'; // tag after the current crumb
    /* === END OF OPTIONS === */

    if ( function_exists( 'falkon_get_global_options' ) ) $falkon_option = falkon_get_global_options();

    global $post;
    $home_link      = home_url('/');
    $blog_link    = get_permalink( get_option( 'page_for_posts' ) );
    $link_before    = '<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">';
    $link_after     = '</span>';
    $link_attr      = ' itemprop="url"';
    $link_in_before = '<span itemprop="title">';
    $link_in_after  = '</span>';
    $link           = $link_before . '<a href="%1$s"' . $link_attr . '>' . $link_in_before . '%2$s' . $link_in_after . '</a>' . $link_after;
    $frontpage_id   = get_option('page_on_front');
    $parent_id      = $post->post_parent;
    $delimiter      = ' ' . $delim_before . $delimiter . $delim_after . ' ';

    if (is_home() || is_front_page()) {

        if ($show_on_home == 1) echo '<div class="breadcrumbs"><a href="' . $home_link . '">' . $text['home'] . '</a>'.$delimiter.sprintf($link, $blog_link, $text['blog']) .'</div>';

    } else {

        echo '<div class="breadcrumbs">';
        if ($show_home_link == 1) echo sprintf($link, $home_link, $text['home']);

        if ( is_category() ) {
            $cat = get_category(get_query_var('cat'), false);
            if ($cat->parent != 0) {
                $cats = get_category_parents($cat->parent, TRUE, $delimiter);
                $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
                $cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr .'>' . $link_in_before . '$2' . $link_in_after .'</a>' . $link_after, $cats);
                if ($show_title == 0) $cats = preg_replace('/ title="(.*?)"/', '', $cats);
                if ($show_home_link == 1) echo $delimiter;
                echo $cats;
            }
            if ( get_query_var('paged') ) {
                $cat = $cat->cat_ID;
                echo $delimiter . sprintf($link, get_category_link($cat), get_cat_name($cat)) . $delimiter . $before . sprintf($text['page'], get_query_var('paged')) . $after;
            } else {
                if ($show_current == 1) echo $delimiter . $before . sprintf($text['category'], single_cat_title('', false)) . $after;
            }

        } elseif ( is_search() ) {
            if ($show_home_link == 1) echo $delimiter;
            echo $before . sprintf($text['search'], get_search_query()) . $after;

        } elseif ( is_day() ) {
            if ($show_home_link == 1) echo $delimiter;
            echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
            echo sprintf($link, get_month_link(get_the_time('Y'), get_the_time('m')), get_the_time('F')) . $delimiter;
            echo $before . get_the_time('d') . $after;

        } elseif ( is_month() ) {
            if ($show_home_link == 1) echo $delimiter;
            echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
            echo $before . get_the_time('F') . $after;

        } elseif ( is_year() ) {
            if ($show_home_link == 1) echo $delimiter;
            echo $before . get_the_time('Y') . $after;

        } elseif ( is_single() && !is_attachment() ) {
            if ($show_home_link == 1) echo $delimiter;
            if ( get_post_type() != 'post' ) {
                $post_type = get_post_type_object(get_post_type());
//                var_dump($post_type);
                if($post_type->name == 'rns-post'){


                    $original_post = $post;
                    $post = get_post($falkon_option['falkon_rns_root_page_id']);
                    $parent_id      = $post->post_parent;

                    if ($parent_id != $frontpage_id) {
                        $breadcrumbs = array();
                        while ($parent_id) {
                            $page = get_page($parent_id);
                            if ($parent_id != $frontpage_id) {
                                $breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
                            }
                            $parent_id = $page->post_parent;
                        }
                        $breadcrumbs = array_reverse($breadcrumbs);
                        for ($i = 0; $i < count($breadcrumbs); $i++) {
                            echo $breadcrumbs[$i];
                            if ($i != count($breadcrumbs)-1) echo $delimiter;
                        }
                    }


//                $breadcrumbs[] = ;
                    echo $delimiter .sprintf($link, get_permalink($falkon_option['falkon_rns_root_page_id']), get_the_title($falkon_option['falkon_rns_root_page_id']));
                    if(is_tax()){
                        $this_cat = get_category(get_query_var('document_category'), false);
                        if ($this_cat->parent != 0) {
                            $cats = get_category_parents($this_cat->parent, TRUE, $delimiter);
                            if ($show_current == 0) $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
                            $cats = str_replace('<a', $link_before . '<a' . $link_attr, $cats);
                            $cats = str_replace('</a>', '</a>' . $link_after, $cats);
                            if ($show_title == 0) $cats = preg_replace('/ title="(.*?)"/', '', $cats);
                            echo $cats;
                        }
                        if ($show_current == 1) echo $delimiter. $before . sprintf($text['document_category'], single_cat_title('', false)) . $after;
                    }
                    //echo $delimiter .$before . $post_type->labels->name . $after;
                    wp_reset_postdata();
                }
                elseif($post_type->name == 'job'){


                    $original_post = $post;
                    $post = get_post($falkon_option['falkon_job_root_page_id']);
                    $parent_id      = $post->post_parent;

                    if ($parent_id != $frontpage_id) {
                        $breadcrumbs = array();
                        while ($parent_id) {
                            $page = get_page($parent_id);
                            if ($parent_id != $frontpage_id) {
                                $breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
                            }
                            $parent_id = $page->post_parent;
                        }
                        $breadcrumbs = array_reverse($breadcrumbs);
                        for ($i = 0; $i < count($breadcrumbs); $i++) {
                            echo $breadcrumbs[$i];
                            if ($i != count($breadcrumbs)-1) echo $delimiter;
                        }
                    }
//                $breadcrumbs[] = ;
                    echo $delimiter .sprintf($link, get_permalink($falkon_option['falkon_job_root_page_id']), get_the_title($falkon_option['falkon_job_root_page_id']));
                    if(is_tax()){
                        $this_cat = get_category(get_query_var('document_category'), false);
                        if ($this_cat->parent != 0) {
                            $cats = get_category_parents($this_cat->parent, TRUE, $delimiter);
                            if ($show_current == 0) $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
                            $cats = str_replace('<a', $link_before . '<a' . $link_attr, $cats);
                            $cats = str_replace('</a>', '</a>' . $link_after, $cats);
                            if ($show_title == 0) $cats = preg_replace('/ title="(.*?)"/', '', $cats);
                            echo $cats;
                        }
                        if ($show_current == 1) echo $delimiter. $before . sprintf($text['document_category'], single_cat_title('', false)) . $after;
                    }
                    //echo $delimiter .$before . $post_type->labels->name . $after;
                    wp_reset_postdata();
                }
                else{
                    $slug = $post_type->rewrite;
                    printf($link, $home_link . '/' . $slug['slug'] . '/', $post_type->labels->singular_name);

                }
                //Get custom title if any, else use get_the_title
                global $page_mb;
                $page_meta = $page_mb->the_meta();
                $page_title = $page_meta['custom_title']? $page_meta['custom_title']: get_the_title();
                if(isset($_GET['job-id']) and trim($_GET['job-id'])!=''){
                    $job_id = (int)$_GET['job-id'];
                    if($job_id!=0)
                        $page_title .= ' - '.get_the_title($job_id);
                }
                if ($show_current == 1) echo $delimiter . $before . $page_title . $after;

            } else {
                $cat = get_the_category(); $cat = $cat[0];
                $cats = get_category_parents($cat, TRUE, $delimiter);
                if ($show_current == 0 || get_query_var('cpage')) $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
                $cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr .'>' . $link_in_before . '$2' . $link_in_after .'</a>' . $link_after, $cats);
                if ($show_title == 0) $cats = preg_replace('/ title="(.*?)"/', '', $cats);
                echo $cats;
                if ( get_query_var('cpage') ) {
                    echo $delimiter . sprintf($link, get_permalink(), get_the_title()) . $delimiter . $before . sprintf($text['cpage'], get_query_var('cpage')) . $after;
                } else {
                    if ($show_current == 1) echo $before . get_the_title() . $after;
                }
            }

            // custom post type
        } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
            $post_type = get_post_type_object(get_post_type());
            if ( get_query_var('paged') ) {
                echo $delimiter . sprintf($link, get_post_type_archive_link($post_type->name), $post_type->label) . $delimiter . $before . sprintf($text['page'], get_query_var('paged')) . $after;
            } else {
                if ($show_current == 1) echo $delimiter . $before . $post_type->label . $after;
            }

        } elseif ( is_attachment() ) {
            if ($show_home_link == 1) echo $delimiter;
            $parent = get_post($parent_id);
            $cat = get_the_category($parent->ID); $cat = $cat[0];
            if ($cat) {
                $cats = get_category_parents($cat, TRUE, $delimiter);
                $cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr .'>' . $link_in_before . '$2' . $link_in_after .'</a>' . $link_after, $cats);
                if ($show_title == 0) $cats = preg_replace('/ title="(.*?)"/', '', $cats);
                echo $cats;
            }
            //printf($link, get_permalink($parent), $parent->post_title);
            if ($show_current == 1) echo $delimiter . $before . get_the_title() . $after;

        } elseif ( is_page() && !$parent_id ) {
            //Get custom title if any, else use get_the_title
            global $page_mb;
            $page_meta = $page_mb->the_meta();
            $page_title = $page_meta['custom_title']? $page_meta['custom_title']: get_the_title();
            if ($show_current == 1) echo $delimiter . $before . $page_title . $after;

        } elseif ( is_page() && $parent_id ) {
            if ($show_home_link == 1) echo $delimiter;
            if ($parent_id != $frontpage_id) {
                $breadcrumbs = array();
                while ($parent_id) {
                    $page = get_page($parent_id);
                    if ($parent_id != $frontpage_id) {
                        //Get custom title if any, else use get_the_title
                        global $page_mb;
                        $page_meta = $page_mb->the_meta($page->ID);
                        $page_title = $page_meta['custom_title']? $page_meta['custom_title']: get_the_title($page->ID);
                        $breadcrumbs[] = sprintf($link, get_permalink($page->ID), $page_title);
                    }
                    $parent_id = $page->post_parent;
                }
                $breadcrumbs = array_reverse($breadcrumbs);
                for ($i = 0; $i < count($breadcrumbs); $i++) {
                    echo $breadcrumbs[$i];
                    if ($i != count($breadcrumbs)-1) echo $delimiter;
                }
            }
            //Get custom title if any, else use get_the_title
            global $page_mb;
            $page_meta = $page_mb->the_meta();
            $page_title = $page_meta['custom_title']? $page_meta['custom_title']: get_the_title();
            if ($show_current == 1) echo $delimiter . $before . $page_title . $after;

        } elseif ( is_tag() ) {
            if ($show_current == 1) echo $delimiter . $before . sprintf($text['tag'], single_tag_title('', false)) . $after;

        } elseif ( is_author() ) {
            if ($show_home_link == 1) echo $delimiter;
            global $author;
            $author = get_userdata($author);
            echo $before . sprintf($text['author'], $author->display_name) . $after;

        } elseif ( is_404() ) {
            if ($show_home_link == 1) echo $delimiter;
            echo $before . $text['404'] . $after;

        } elseif ( has_post_format() && !is_singular() ) {
            if ($show_home_link == 1) echo $delimiter;
            echo get_post_format_string( get_post_format() );
        }

        echo '</div><!-- .breadcrumbs -->';

    }
} // end dimox_breadcrumbs()


function flkn_news_posted_on() {
    printf( __( '<span class="%1$s">%2$s</span>', 'falkontextdomain' ),
        'meta-prep meta-prep-author',
        sprintf( '<a href="%1$s" title="%2$s" rel="bookmark nofollow"><span class="entry-date">%3$s</span></a>',
            get_permalink(),
            esc_attr( get_the_time('H:i') ),
            get_the_date('jS F Y')
        ),
        sprintf( 'Posted by <span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s" rel="nofollow">%3$s</a></span>',
            get_author_posts_url( get_the_author_meta( 'ID' ) ),
            sprintf( esc_attr__( 'View all posts by %s', 'twentyten' ), get_the_author() ),
            get_the_author()
        )
    );
}

function flkn_read_more() {
    printf( __( '%1$s', 'twentyten' ),

        sprintf( '<a href="%1$s" title="%2$s" rel="nofollow" class="button btnrm">%3$s</a>',get_permalink(),esc_attr( get_the_title() ),	'Read More...'
        )
    );
}


// Sub page list - echoed in file subpg-list.php
function has_children() {
    global $post;

    $pages = get_pages('child_of=' . $post->ID);

    return count($pages);
}
function is_top_level() {
    global $post, $wpdb;

    $current_page = $wpdb->get_var("SELECT post_parent FROM $wpdb->posts WHERE ID = " . $post->ID);

    return $current_page;
}