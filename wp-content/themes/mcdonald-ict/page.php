<?php
get_template_part('includes/header');

global $norotbanner_mb;
$nrbanner_meta = $norotbanner_mb->the_meta();

global $grybx_meta;
$grybx = $grybx_meta->the_meta();

// Falkon Options
global $falkon_option;
?>

<section id="pge-banner">
	<?php
	if (isset( $nrbanner_meta['imgurl'])) {
		echo '<div class="banner-nrt">';
		echo '<img src="' . $nrbanner_meta['imgurl'] . '"/>';
		echo '<div class="banner-nrt-title hidden-xs">';
		echo '<span class="title">' . $nrbanner_meta['title_line'] . '</span>';
		echo '<span>' . $nrbanner_meta['ban_tag'] . '</span>';
		echo '</div>';
		echo '</div>';
	}
	else  {
		echo '<div class="no-img-upl">';
		echo '<img src="' . get_template_directory_uri() . '/images/default-inner-banner.jpg" class="blog-banner-bl"/>';
		echo '<div class="banner-nrt-title hidden-xs">';
		echo '<span class="title">No Banner image Uploaded</span>';
		echo '<span>Add one in this page\'s settings</span>';
		echo '</div>';
		echo '</div>';
	}
	?>
</section>

<?php get_template_part( 'includes/blocks/subpg', 'list' ); ?>

<section id="page-content">
	<div class="container">

			<div class="col-xs-12 col-sm-12">
				<?php
				if (isset( $general_meta['gen-title'])) {
					echo '<h1>'.$general_meta['gen-title'].'</h1>';
				}
				else {
                    echo '<h1>'. get_the_title() .'</h1>';
				}
				?>

<!--				--><?php
//				if (isset( $general_meta['gen-tag'])) {
//					echo '<span class="gen-sub-title">' . $general_meta['gen-tag'] . '</span>';
//				}
//				?>
				</div>
				<br><br>
				<?php get_template_part('includes/loops/content-page-ntitle'); ?>

	</div>
</section>


<?php
if(isset( $grybx['grey_title'], $grybx['grey_content'])) {
?>
<section id="second-page-content">
        <div class="container">
            <div class="col-xs-12 col-sm-12">
                <h2><?php echo $grybx['grey_title']?></h2>
<?php echo wpautop(do_shortcode($grybx['grey_content']))  ?>

            </div>
        </div>
    </section>
    <?php
}
else  {

}
?>
<!-- END footer CTA -->




<?php get_template_part('includes/footer'); ?>
