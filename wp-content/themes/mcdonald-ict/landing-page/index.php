<?php
/*
*
*/
ini_set( "display_errors", false );					//SET TO FALSE WHEN LIVE!
date_default_timezone_set( "Europe/London" );  		//http://www.php.net/manual/en/timezones.php

function isValidEmail($email){
    return preg_match("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^", $email);
}
$email_feedback = array('','');
$postNoticeEmail = 'Your email address';
$emailValid = true;
$formSent = false;
if(isset($_POST['submit'])){
	$postNoticeEmail = $_POST['noticeemail'];
	if($postNoticeEmail =='Your email address' or strlen($postNoticeEmail>255) or isValidEmail($postNoticeEmail)==0) $emailValid = false;

	if($emailValid and !$formSent){
		$f=fopen("5YfbCCzuNNSmmxDPNg2w.txt","a+");
		fwrite($f,$postNoticeEmail.','.date("H:i:s d-m-Y",time()).','.$_SERVER['REMOTE_ADDR'].','.$_SERVER['HTTP_REFERER']."\r\n");
		fclose($f);
		$postNoticeEmail = 'Your email address';
		$formSent = true;
	}
}
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en-US"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<title>McDonald ICT</title>
	<meta name="copyright" content="Copyright (c) <?php echo date('Y');?> Arnold Series UK" />
    <meta name="description" content="" />
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="stylesheet" type="text/css" media="all" href="css/style.css" />
	<?php /*?><link rel="shortcut icon" href="images/favicon.ico"><?php */?>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <script src="js/jquery-1.8.3.min.js"></script>
	<script src="js/bootstrap/alert.js"></script>
	<script type="text/javascript">
    $(document).ready(function() {
		$('input:text#noticeemail').focus(
			function(){
				if($(this).val()=='Your email address' || $(this).change()) $(this).removeClass('error');
			});
    });
	</script>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body class="holdingpage">
	<div class="container-fluid" id="arnold-wrap">
		<div class="row">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
						<img src="images/logo.png" class="logo img-responsive center-block" />
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
						<div class="col-xs-12">
							<div class="row">
								<h1>The new McDonald ICT website is nearly ready</h1>
							</div>
						</div>
						<div class="main-box col-xs-12">
							<div class="col-xs-12">
								<div class="box-section border">
									<p>McDonald ICT is an established ICT company setup specifically to help start up companies and SME’s with their IT requirements in the North West.  The new website is coming soon, but for more information contact us using the details below or sign up to be notified when the site goes live.</p>
								</div>
							</div>
							<div class="col-xs-12">
								<div class="box-section border contact">
									<div class="row">
										<div class="col-sm-6">
											<div class="item"><span class="fa fa-envelope-o"></span><a href="mailto:hello@mcdonaldict.com">hello@mcdonaldict.com</a></div>
											<div class="item"><span class="fa fa-phone"></span>0161 660 5445</div>
											<div class="item"><span class="fa fa-fax"></span>0161 660 5446</div>
										</div>
										<div class="col-sm-6">
											<span class="fa fa-building"></span>
											<div class="address item">
												<strong>McDonald ICT Ltd</strong><br />
												Paul House<br />
												Stockport Road<br />
												Timperley<br />
												Altrincham<br />
												WA15 7UQ<br />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12">
								<div class="box-secion">
									<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13457.771162808645!2d-2.324584799204751!3d53.39657653592846!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487bace45bdb16ab%3A0x554d0a802ca04336!2sPaul+House%2C+Timperley%2C+Altrincham!5e0!3m2!1sen!2suk!4v1469700029470" width="510" height="310" frameborder="0" style="border:0" allowfullscreen></iframe>
								</div>
							</div>
							<div class="col-xs-12">
								<div class="box-section">
									<div class="form">
										<p class="text-center l-text">Sign up to be notified when it goes live!</p>
										<form role="form" method="post" action="" enctype="application/x-www-form-urlencoded" id="notified">
											<div class="row">
												<div class="form-group col-sm-8<?php echo $email_feedback[0]?>">
													<label for="noticeemail" class="sr-only">Email:</label>
													<input type="text" name="noticeemail" class="form-control" id="noticeemail" value="<?php echo $postNoticeEmail;?>" onfocus="if(this.value == 'Your email address'){this.value = '';}" onblur="if(this.value == ''){this.value = 'Your email address';}" maxlength="255" placeholder="Your email address" aria-required="true">
													<?php echo $email_feedback[1]?>
												</div>
												<div class="col-sm-4">
													<button type="submit" name="submit" id="submit" class="btn btn-primary col-xs-12">Sign Up</button>
												</div>
												<div class="col-xs-12">
													<?php if($formSent): ?>
														<div class="alert alert-success">
															<a href="#" class="close" data-dismiss="alert">&times;</a>
															<strong>Thank you!</strong> We have saved your details!
														</div>
													<?php elseif(isset($_POST['submit']) and !$formSent): ?>
														<div class="alert alert-danger">
															<a href="#" class="close" data-dismiss="alert">&times;</a>
															<strong>Error!</strong> Please check errors and submit the form again!
														</div>
													<?php endif; ?>
													<?php
													if(!$emailValid){
														$email_feedback = array(' has-error has-feedback', '<span class="glyphicon glyphicon-remove form-control-feedback"></span>'
														);
													}
													elseif(isset($_POST['submit']) and !$formSent and $emailValid){
														$email_feedback = array(' has-success has-feedback', '<span class="glyphicon glyphicon-ok form-control-feedback"></span>'
														);
													}
													?>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>