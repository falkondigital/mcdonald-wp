<?php get_template_part('includes/header');

 // Falkon Options
 global $falkon_option;

global $genpage_meta;
$page_att = $genpage_meta->the_meta();

global $accordion_meta;
$acc_meta = $accordion_meta->the_meta();

?>




<?php get_template_part( 'includes/blocks/home', 'slider' ); ?>



<!---->
<!--<section id="white-txt-bg">-->
<!--	<div class="container">-->
<!---->
<!---->
<!--			<div class="col-xs-12 col-sm-12 text-center">-->
<!---->
<!--				--><?php
//					echo '<h2>' . $page_att['custom-title'] . '</h2>';
//				?>
<!--				--><?php //get_template_part('includes/loops/content', get_post_format()); ?>
<!--			</div>-->
<!---->
<!---->
<!--	</div><!-- /.container -->
<!--</section>-->


<!-- Tab panel -->
<section id="home-tabs-bg">
	<div id="accordion-home">
	  <div class="container">

		<div class="row">
		<div data-toggle="pill" href="#start-up" class="col-xs-12 col-sm-4 home-panel pan-active">
			<a href="">
				<i class="fa <?php echo $acc_meta['font_awesome_ico']?>" aria-hidden="true"></i><br><br>
				<span><?php echo $acc_meta['tab_title']?></span>
			</a>
			<hr>
		</div>
		<div data-toggle="pill" href="#start-up4" class="col-xs-12 col-sm-4 home-panel">
			<a href="">
				<i class="fa <?php echo $acc_meta['font_awesome_ico2']?>" aria-hidden="true"></i><br><br>
				<span><?php echo $acc_meta['tab_title2']?></span>
				</a>
			<hr>
		</div>
		<div data-toggle="pill" href="#start-up10" class="col-xs-12 col-sm-4 home-panel">
			<a href="">
				<i class="fa <?php echo $acc_meta['font_awesome_ico3']?>" aria-hidden="true"></i><br><br>
				<span><?php echo $acc_meta['tab_title3']?></span>
			</a>
			<hr>
		</div>
		</div>


		  <div class="row">
		  <div class="tab-content">
			<div id="start-up" class="tab-pane fade in active">
					<h3><?php echo $acc_meta['tab_title']?></h3>
					<p>
						<?php echo wpautop(do_shortcode($acc_meta['tab_desc']))  ?>
					</p>
			</div>
			<div id="start-up4" class="tab-pane fade">
					<h3><?php echo $acc_meta['tab_title2']?></h3>
					<p>
						<?php echo wpautop(do_shortcode($acc_meta['tab_desc2']))  ?>
					</p>
			</div>
			<div id="start-up10" class="tab-pane fade">
					<h3><?php echo $acc_meta['tab_title3']?></h3>
					<p>
						<?php echo wpautop(do_shortcode($acc_meta['tab_desc3']))  ?>
					</p>
			</div>
		</div>
	    </div>

        <div class="row pd-top">
            <div class="col-xs-12 col-sm-offset-4 col-sm-4 col-md-offset-4 col-md-4 col-lg-offset-5 col-lg-2">
                <a class="btn button btn-mcd" href="#">Find Out More</a>
            </div>
        </div>

	  </div>
	</div>
</section>




<?php get_template_part('includes/footer'); ?>