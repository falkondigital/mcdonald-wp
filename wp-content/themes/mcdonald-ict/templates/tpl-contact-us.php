<?php
/*Template Name: Contact us */
get_template_part('includes/header');
// Falkon Options
global $falkon_option;

//ar_dump($falkon_option);

echo '<iframe style="height:320px; width:100%;border:none;" src="' .$falkon_option['falkon_contact_gmap'].' "></iframe>';


echo '<section id="white-txt-bg">';
echo '<div class="container">';
echo '<div class="row">';
echo '<div class="col-xs-12 col-sm-12 text-center">';
get_template_part('includes/loops/content-custom-title');
echo '</div>';
echo '</div>';
echo '</div>';
echo '</section>';


echo '<section id="grey-txt-bg">';
echo '<div class="container">';
echo '<div class="row">';
?>
    <div class="col-xs-12 col-sm-5 col-md-4 contact-info">
        <?php
//           var_dump($falkon_option);
               if($falkon_option['falkon_contact_email']) echo '<i style="color:#0771b8;" class="fa fa-envelope fa-fw" aria-hidden="true"></i> <a href="mailto:'.$falkon_option['falkon_contact_email'].'"><strong style="color:#0771b8;">'.$falkon_option['falkon_contact_email'] .'</strong></a><br>';
               if($falkon_option['falkon_contact_number']) echo '<i class="fa fa-phone fa-fw" aria-hidden="true"></i> '.$falkon_option['falkon_contact_number'].'<br>';
               if($falkon_option['falkon_secondary_contact_number']) echo '<i class="fa fa-fax fa-fw" aria-hidden="true"></i> '.$falkon_option['falkon_secondary_contact_number'].'<br>';
               echo '<span style="padding-left:25px;display:inline-block;"><i class="fa fa-building fa-fw" aria-hidden="true" style="margin-left:-25px;"></i> '.$falkon_option['falkon_contact_pg_address'].'</span>';
        ?>
    </div>
    <div class="col-xs-12 col-sm-7 col-md-8">
        <?php
        echo do_shortcode("[contact-form-7 id='1670' title='Contact Form']");
        ?>
    </div>

<?php
echo '</div>';
echo '</div>';
echo '</section>';
get_template_part('includes/footer');

?>