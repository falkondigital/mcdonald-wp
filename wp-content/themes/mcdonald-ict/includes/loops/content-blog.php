<?php
/*
The Blog  Loop (used by index.php)
=====================================================
*/
?>
<div class="row">
<?php if(have_posts()): while(have_posts()): the_post();?>
<div class="col-xs-12 col-sm-6">
	<div class="col-xs-12 col-sm-12 blg-listing np">
	<article role="article" id="post_<?php the_ID()?>">

     <a href="<?php the_permalink()?>">
			<?php the_post_thumbnail('full', array('class'=>'blg-listing-img')); ?>
     </a>
	 <div class="text-center">
		 <?php

		   echo '<h3>' . get_the_title() . '</h3>';
		   echo '<span>' . get_the_date() . '</span>';
		   echo  '<p>' . get_the_excerpt() . '</p>';
		   echo '<a href="'. get_the_permalink() .'"><button class="btn-blue">Read More...</a>';

		 ?>


     </div>

	</article>
	</div>
</div>
<?php endwhile; ?>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12" style="text-align: center;">
		<div style="background-color: #fff; line-height: 0; margin-bottom:25px;">
	<?php if ( function_exists('bst_pagination') ) { bst_pagination(); } else if ( is_paged() ) { ?>

		  <ul class="pagination">
			<li class="older"><?php next_posts_link('<i class="glyphicon glyphicon-arrow-left"></i> ' . __('Previous', 'bst')) ?></li>
			<li class="newer"><?php previous_posts_link(__('Next', 'bst') . ' <i class="glyphicon glyphicon-arrow-right"></i>') ?></li>
		</ul>

	<?php } ?>
		</div>
	</div>
</div>

<?php else: get_template_part('includes/loops/content', 'none'); endif; ?>
