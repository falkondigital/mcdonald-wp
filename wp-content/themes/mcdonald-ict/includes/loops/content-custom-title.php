<?php
/*
This loop calls title from custom page attributes (WPalchemy)
*/
global $genpage_meta;
$pg_att = $genpage_meta->the_meta();
?>

<?php echo '<h1>' . $pg_att['custom-title'] . '</h1>';?>
<hr class="underlined">
<?php if(have_posts()): while(have_posts()): the_post(); ?>
	<article role="article" id="post_<?php the_ID()?>" <?php post_class()?>>
		<?php the_content()?>
		<?php wp_link_pages(); ?>
	</article>
<?php endwhile; ?>
<?php else: get_template_part('includes/loops/content', 'none'); ?>
<?php endif; ?>
