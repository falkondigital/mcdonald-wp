<?php
/**
 * Define page tabs
 * $tabs['tab-slug'] 	= __('Tab Name', 'wpShop');
 */
function falkon_options_two_page_tabs() {
	
	$tabs = array();

    $tabs['homepage'] 		= __('Homepage', 'falkon_textdomain');
	$tabs['general'] 	= __('General Settings', 'falkon_textdomain');
    $tabs['page-ids'] 		= __('Page IDs', 'falkon_textdomain');
	$tabs['social-media'] 		= __('Social Media', 'falkon_textdomain');
	$tabs['contacts'] 		= __('Contact Settings', 'falkon_textdomain');
    $tabs['smtp_email'] 		= __('Email Settings', 'falkon_textdomain');
    //$tabs['select'] 		= __('Select', 'wpShop');
	//$tabs['checkboxes'] 	= __('Checkboxes', 'wpShop');
	
	return $tabs;
}

/**
 * Define our settings sections
 *
 * array key=$id, array value=$title in: add_settings_section( $id, $title, $callback, $page );
 * @return array
 */
function falkon_options_two_page_sections() {
	
	// get the current tab
	$tab = falkon_get_the_tab();

	// sections according to tab
	switch ($tab) {
		// Text Inputs
		case 'general':
			$sections = array();
			//$sections['hp_section']				= __('Homepage Settings', 'falkon_textdomain');
			$sections['footer_section']			= __('Footer Settings', 'falkon_textdomain');
			$sections['general_section']			= __('Other Settings', 'falkon_textdomain');
            //$sections['advert_section']				= __('Advert Section', 'falkon_textdomain');
		break;
        case 'homepage':
            $sections = array();
            //$sections['hp_section']				= __('Homepage Settings', 'falkon_textdomain');
//            $sections['event_section']				= __('Event Section', 'falkon_textdomain');
//            $sections['feat_art_section']				= __('Featured Artiste Section', 'falkon_textdomain');
//            $sections['showreel_section']				= __('Showreel Section', 'falkon_textdomain');

            //$sections['footer_section']			= __('Footer Settings', 'falkon_textdomain');
            //$sections['general_section']			= __('General Settings', 'falkon_textdomain');
        break;
        case 'page-ids':
            $sections = array();
//            $sections['dealer_signup_ids_section'] 	= __('Dealer Signup Page ID Links', 'falkon_textdomain');
//            $sections['portal_ids_section'] 	= __('Portal Page ID Links', 'falkon_textdomain');
            $sections['loginregister_ids_section'] 	= __('Login/Reg Page ID Links', 'falkon_textdomain');
            $sections['dashboard_ids_section'] 	= __('Dashboard Page ID Links', 'falkon_textdomain');
            $sections['promo_ids_section'] 	= __('Promo Page ID Links', 'falkon_textdomain');
            $sections['general_ids_section'] 	= __('General Page ID Links', 'falkon_textdomain');
            break;
        case 'social-media':
			$sections = array();
			$sections['social_footer_section'] 	= __('Social Media Footer Links', 'falkon_textdomain');
		break;
		
		case 'contacts':
			$sections = array();
			$sections['contact_section']    =   __('Site Contact details', 'falkon_textdomain');

		break;
        case 'smtp_email':
            $sections = array();
            $sections['email_section'] 	    =   __('Email Sending Options', 'falkon_textdomain');
            $sections['smtp_email_section'] 	    =   __('SMTP Email Sending Options', 'falkon_textdomain');
            break;


	}
	
	return $sections;	
} 

/**
 * Define our form fields (options) 
 *
 * @return array
 */
function falkon_options_two_page_fields() {
	
	// get the current tab
	$tab = falkon_get_the_tab();

    $pagepost_list = get_pages();
    $page_array = array();
    array_push($page_array,'Please Select|');
    foreach ( $pagepost_list as $page ) {
        $page_ancestors = get_post_ancestors( $page->ID );
        $level_deep = count($page_ancestors);
        $delim = str_repeat("— ", ($level_deep));
        if($page->post_parent!=0) $page->post_title = $delim.' '.$page->post_title;
        array_push($page_array,$page->post_title.'|'.$page->ID);
    }
	
	// setting fields according to tab
	switch ($tab) {
        case 'homepage':
            $options[] = array(
                "section" => "event_section",
                "id"      => FALKON_SHORTNAME . "_homepage_event_vis",
                "title"   => __( 'Artiste Event Section', 'falkondigital' ),
                "desc"    => __( 'Show or hide this section on the homepage.', 'falkon_textdomain' ),
                "type"    => "radio",
                "std"       =>  "1",
                'choices' => array(
                    'Visible|1',
                    'Not visible|0',
                )
            );
            $options[] = array(
                "section" => "event_section",
                "id"      => FALKON_SHORTNAME . "_homepage_event_title",
                "title"   => __( 'Homepage Event Title', 'falkondigital' ),
                "desc"    => __( 'Title for homepage event block, default is "BROWSE BY EVENT TYPE".', 'falkondigital' ),
                "type"    => "text",
                "std"     => __('Browse By Event Type','falkondigital'),
                "class"   => "nohtml"
            );

            $taxonomy     = 'stock_event';
            $orderby      = 'slug';
            $show_count   = 0;      // 1 for yes, 0 for no
            $pad_counts   = 0;      // 1 for yes, 0 for no
            $hierarchical = 1;      // 1 for yes, 0 for no
            $args = array(
                'taxonomy'     => $taxonomy,
                'orderby'      => $orderby,
                'order'         =>'ASC',
                'show_count'   => $show_count,
                'pad_counts'   => $pad_counts,
                'hierarchical' => $hierarchical,
                'hide_empty' => 0,
                'parent' => 0
            );
            $yourtaxonomies = get_categories($args);
            $tax_data = get_taxonomy( $taxonomy );
            $tax_data = $tax_data->labels;
            $tax_array = array();
            array_push($tax_array,'Please Select|');
            if(is_object($tax_data)){
                foreach($yourtaxonomies as $category) {
                    $categories_sub = get_categories( array(
                            'parent'	=>	$category->term_id,
                            'taxonomy'     => $taxonomy,
                            'hide_empty'	=>	false,
                            'hierarchical'	=>	false,
                            'pad_counts'	=>	true,
                        )
                    );
                    array_push($tax_array,$category->name.'|'.$category->term_id);
                    if(!empty($categories_sub)){
                        foreach($categories_sub as $category_sub) {
                            $categories_sub_sub = get_categories( array(
                                    'parent'	=>	$category_sub->term_id,
                                    'taxonomy'     => $taxonomy,
                                    'orderby'      => $orderby,
                                    'hide_empty'	=>	false,
                                    'hierarchical'	=>	false,
                                    'pad_counts'	=>	true,
                                )
                            );
                            array_push($tax_array,'&mdash;&nbsp;&nbsp;'.$category_sub->name.'|'.$category_sub->term_id);
                            if(!empty($categories_sub_sub)){
                                foreach($categories_sub_sub as $category_sub_sub) {
                                    array_push($tax_array,'&nbsp;&nbsp;&nbsp;&nbsp;&ndash;&nbsp;&nbsp;'.$category_sub_sub->name.'|'.$category_sub_sub->term_id);
                                }
                            }
                        }
                    }
                }
            }

            $options[] = array(
                "section" => "event_section",
                "id"      => FALKON_SHORTNAME . "_homepage_event_blocks",
                "title"   => __( 'Event Blocks', 'falkon_textdomain' ),
                "type"    => "select3",
                "std"     => 6,
                "class"     =>  3,
                "choices" => $tax_array,
                "desc"      =>  __( 'The 6 event blocks on the homepage. Select what goes where.', 'falkondigital' )
            );

            //Feat Artiste Cats
            $options[] = array(
                "section" => "feat_art_section",
                "id"      => FALKON_SHORTNAME . "_homepage_feat_art_vis",
                "title"   => __( 'Featured Artiste Section', 'falkondigital' ),
                "desc"    => __( 'Show or hide this section on the homepage.', 'falkon_textdomain' ),
                "type"    => "radio",
                "std"       =>  "1",
                'choices' => array(
                    'Visible|1',
                    'Not visible|0',
                )
            );
            $options[] = array(
                "section" => "feat_art_section",
                "id"      => FALKON_SHORTNAME . "_homepage_feat_art_title",
                "title"   => __( 'Homepage Featured Artiste Title', 'falkondigital' ),
                "desc"    => __( 'Title for homepage featured stock block, default is "Featured Artiste".', 'falkondigital' ),
                "type"    => "text",
                "std"     => __('Featured Artiste','falkondigital'),
                "class"   => "nohtml"
            );

            $taxonomy     = 'stock_genre';
            $orderby      = 'slug';
            $show_count   = 0;      // 1 for yes, 0 for no
            $pad_counts   = 0;      // 1 for yes, 0 for no
            $hierarchical = 1;      // 1 for yes, 0 for no
            $args = array(
                'taxonomy'     => $taxonomy,
                'orderby'      => $orderby,
                'order'         =>'ASC',
                'show_count'   => $show_count,
                'pad_counts'   => $pad_counts,
                'hierarchical' => $hierarchical,
                'hide_empty' => 0,
                'parent' => 0
            );
            $yourtaxonomies = get_categories($args);
            $tax_data = get_taxonomy( $taxonomy );
            $tax_data = $tax_data->labels;
            $tax_array = array();
            array_push($tax_array,'Please Select|');
            if(is_object($tax_data)){
                foreach($yourtaxonomies as $category) {
                    $categories_sub = get_categories( array(
                            'parent'	=>	$category->term_id,
                            'taxonomy'     => $taxonomy,
                            'hide_empty'	=>	false,
                            'hierarchical'	=>	false,
                            'pad_counts'	=>	true,
                        )
                    );
                    array_push($tax_array,$category->name.'|'.$category->term_id);
                    if(!empty($categories_sub)){
                        foreach($categories_sub as $category_sub) {
                            $categories_sub_sub = get_categories( array(
                                    'parent'	=>	$category_sub->term_id,
                                    'taxonomy'     => $taxonomy,
                                    'orderby'      => $orderby,
                                    'hide_empty'	=>	false,
                                    'hierarchical'	=>	false,
                                    'pad_counts'	=>	true,
                                )
                            );
                            array_push($tax_array,'&mdash;&nbsp;&nbsp;'.$category_sub->name.'|'.$category_sub->term_id);
                            if(!empty($categories_sub_sub)){
                                foreach($categories_sub_sub as $category_sub_sub) {
                                    array_push($tax_array,'&nbsp;&nbsp;&nbsp;&nbsp;&ndash;&nbsp;&nbsp;'.$category_sub_sub->name.'|'.$category_sub_sub->term_id);
                                }
                            }
                        }
                    }
                }
            }

            $options[] = array(
                "section" => "feat_art_section",
                "id"      => FALKON_SHORTNAME . "_homepage_feat_art_blocks",
                "title"   => __( 'Event Blocks', 'falkon_textdomain' ),
                "type"    => "select3",
                "std"     => 6,
                "class"     =>  3,
                "choices" => $tax_array,
                "desc"      =>  __( 'The 6 featured stock blocks on the homepage. Select what goes where.', 'falkondigital' )
            );

            //Showreel
            $options[] = array(
                "section" => "showreel_section",
                "id"      => FALKON_SHORTNAME . "_homepage_showreel_vis",
                "title"   => __( 'Showreel Section', 'falkondigital' ),
                "desc"    => __( 'Show or hide this section on the homepage.', 'falkon_textdomain' ),
                "type"    => "radio",
                "std"       =>  "1",
                'choices' => array(
                    'Visible|1',
                    'Not visible|0',
                )
            );
            $options[] = array(
                "section" => "showreel_section",
                "id"      => FALKON_SHORTNAME . "_homepage_showreel_title",
                "title"   => __( 'Homepage Showreel Title', 'falkondigital' ),
                "desc"    => __( 'Title for homepage showreel section, default is "OUR SHOWREEL".', 'falkondigital' ),
                "type"    => "text",
                "std"     => __('OUR SHOWREEL','falkondigital'),
                "class"   => "nohtml"
            );
            $options[] = array(
                "section" => "showreel_section",
                "id"      => FALKON_SHORTNAME . "_homepage_showreel_title_sub",
                "title"   => __( 'Homepage Showreel Title', 'falkondigital' ),
                "desc"    => __( 'Sub Title for homepage showreel section, default is "Mike Constantia stock showreel".', 'falkondigital' ),
                "type"    => "text",
                "std"     => __('Mike Constantia stock showreel','falkondigital'),
                "class"   => "nohtml"
            );
            $options[] = array(
                "section" => "showreel_section",
                "id"      => FALKON_SHORTNAME . "_homepage_showreel_text",
                "title"   => __( 'Homepage Showreel Text', 'falkondigital' ),
                "desc"    => __( 'Text for homepage showreel section.', 'falkondigital' ),
                "type"    => "textarea",
                "std"     => __('This video includes clips from some of our most popular stocks including Flameoz, Adam Kitch, Allesandria, Urban Soul Orchestra and Wild BOYS to name just a few.','falkondigital'),
                "class"   => "nohtml widefat"
            );


        break;
		case 'general':
            //Feat Client Area


            $options[] = array(
                "section" => "general_section",
                "id"      => FALKON_SHORTNAME . "_google_recaptcha_api_secret",
                "title"   => __( 'Google reCAPTCHA API Secret', 'falkon_textdomain' ),
                "desc"    => __( 'Google reCAPTCHA API Secret. This cannot be shown to the public and is unique. sshhh..!', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => ""
            );
            $options[] = array(
                "section" => "general_section",
                "id"      => FALKON_SHORTNAME . "_contact_address",
                "title"   => __( 'Reward Postal Address', 'falkon_textdomain' ),
                "desc"    => __( 'Add in contact address which will appear in the PDF to post to.', 'falkon_textdomain' ),
                "type"    => "textarea",
                "std"     => "",
                "class"   => "allowlinebreaks"
            );

		
			
			$options[] = array(
				"section" => "footer_section",
				"id"      => FALKON_SHORTNAME . "_contact_visible",
				"title"   => __( 'Contact details visible?', 'falkon_textdomain' ),
				"desc"    => __( 'Make the contact block of text visible in the footer? To edit these details, click on the <a href="'.admin_url('themes.php?page=falkon-settings-page-one&tab=contacts').'">Contact Settings</a> tab above.', 'falkon_textdomain' ),
				"type"    => "select2",
				"std"    => "1",
				"choices" => array( __('Visible','falkon_textdomain') . "|1", __('Not Visible','falkon_textdomain') . "|0")
			);
			
			/*$options[] = array(
				"section" => "footer_section",
				"id"      => FALKON_SHORTNAME . "_copy_visible",
				"title"   => __( 'Copyright text visible?', 'falkon_textdomain' ),
				"desc"    => __( 'Make the copyright block of text visible in the footer?', 'falkon_textdomain' ),
				"type"    => "select2",
				"std"    => "1",
				"choices" => array( __('Visible','falkon_textdomain') . "|1", __('Not Visible','falkon_textdomain') . "|0")
			);*/
            $options[] = array(
                "section" => "footer_section",
                "id"      => FALKON_SHORTNAME . "_footer_blurb",
                "title"   => 'Footer copyright blurb',
                "desc"    => 'Copyright text in the footer for website to the left. Include {{year}} to include the current year in the text.',
                "type"    => "text",
                "std"     => '&copy; {{year}}',
                "class"   => "nohtml"
            );
			$options[] = array(
				"section" => "footer_section",
				"id"      => FALKON_SHORTNAME . "_comp_num",
				"title"   => __( 'Company Number', 'falkon_textdomain' ),
				"desc"    => __( 'Company reg number which goes in the footer of the site. Can also be used throughout the site with shortcode "<strong>[company-reg-num]</strong>"', 'falkon_textdomain' ),
				"type"    => "text",
				"class"   => "nohtml"
			);
			$pagepost_list = get_pages();
			$page_array = array();
			array_push($page_array,'Please Select|');
			foreach ( $pagepost_list as $page ) {
				array_push($page_array,$page->post_title.'|'.$page->ID);
				//$option = '<option value="' . get_page_link( $page->ID ) . '">';
				//$option .= $page->post_title;
				//$option .= '</option>';
				//echo $option;
			  }


			
					
			/*$options[] = array(
				"section" => "txt_section",
				"id"      => FALKON_SHORTNAME . "_txt_input_2",
				"title"   => __( 'Text Input - Some HTML OK!', 'falkon_textdomain' ),
				"desc"    => __( 'A regular text input field. Some inline HTML (&lt;a&gt;, &lt;b&gt;, &lt;em&gt;, &lt;i&gt;, &lt;strong&gt;) is allowed.', 'falkon_textdomain' ),
				"type"    => "text",
				"std"     => __('Some default value','falkon_textdomain')
			);
			
			$options[] = array(
				"section" => "txt_section",
				"id"      => FALKON_SHORTNAME . "_nohtml_txt_input_2",
				"title"   => __( 'No HTML!', 'falkon_textdomain' ),
				"desc"    => __( 'A text input field where no html input is allowed.', 'falkon_textdomain' ),
				"type"    => "text",
				"std"     => __('Some default value','falkon_textdomain'),
				"class"   => "nohtml"
			);
			
			$options[] = array(
				"section" => "txt_section",
				"id"      => FALKON_SHORTNAME . "_numeric_txt_input_2",
				"title"   => __( 'Numeric Input', 'falkon_textdomain' ),
				"desc"    => __( 'A text input field where only numeric input is allowed.', 'falkon_textdomain' ),
				"type"    => "text",
				"std"     => "123",
				"class"   => "numeric"
			);
			
			$options[] = array(
				"section" => "txt_section",
				"id"      => FALKON_SHORTNAME . "_multinumeric_txt_input_2",
				"title"   => __( 'Multinumeric Input', 'falkon_textdomain' ),
				"desc"    => __( 'A text input field where only multible numeric input (i.e. comma separated numeric values) is allowed.', 'falkon_textdomain' ),
				"type"    => "text",
				"std"     => "123,234,345",
				"class"   => "multinumeric"
			);
			
			$options[] = array(
				"section" => "txt_section",
				"id"      => FALKON_SHORTNAME . "_url_txt_input_2",
				"title"   => __( 'URL Input', 'falkon_textdomain' ),
				"desc"    => __( 'A text input field which can be used for urls.', 'falkon_textdomain' ),
				"type"    => "text",
				"std"     => "http://wp.tutsplus.com",
				"class"   => "url"
			);
			
			$options[] = array(
				"section" => "txt_section",
				"id"      => FALKON_SHORTNAME . "_email_txt_input_2",
				"title"   => __( 'Email Input', 'falkon_textdomain' ),
				"desc"    => __( 'A text input field which can be used for email input.', 'falkon_textdomain' ),
				"type"    => "text",
				"std"     => "email@email.com",
				"class"   => "email"
			);
			
			$options[] = array(
				"section" => "txt_section",
				"id"      => FALKON_SHORTNAME . "_multi_txt_input_2",
				"title"   => __( 'Multi-Text Inputs', 'falkon_textdomain' ),
				"desc"    => __( 'A group of text input fields', 'falkon_textdomain' ),
				"type"    => "multi-text",
				"choices" => array( __('Text input 1','falkon_textdomain') . "|txt_input1", __('Text input 2','falkon_textdomain') . "|txt_input2", __('Text input 3','falkon_textdomain') . "|txt_input3", __('Text input 4','falkon_textdomain') . "|txt_input4"),
				"std"     => ""
			);*/
		break;
        case 'page-ids':
            $options[] = array(
                "section" => "dealer_signup_ids_section",
                "id"      => FALKON_SHORTNAME . "_dealer_signup_stage1_page_id",
                "title"   => __( 'Stage 1', 'falkon_textdomain' ),
                "desc"    => __( 'Contact page of site, used in the theme in the header \'View Our UK Locations\'. ', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array//array( __('Visble','falkon_textdomain') . "|true", __('Not Visible','falkon_textdomain') . "|false")
            );
            $options[] = array(
                "section" => "dealer_signup_ids_section",
                "id"      => FALKON_SHORTNAME . "_dealer_signup_stage2_page_id",
                "title"   => __( 'Stage 2', 'falkon_textdomain' ),
                "desc"    => __( 'Contact page of site, used in the theme in the header \'View Our UK Locations\'. ', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array//array( __('Visble','falkon_textdomain') . "|true", __('Not Visible','falkon_textdomain') . "|false")
            );
            $options[] = array(
                "section" => "dealer_signup_ids_section",
                "id"      => FALKON_SHORTNAME . "_dealer_signup_stage3_page_id",
                "title"   => __( 'Stage 3', 'falkon_textdomain' ),
                "desc"    => __( 'Contact page of site, used in the theme in the header \'View Our UK Locations\'. ', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array//array( __('Visble','falkon_textdomain') . "|true", __('Not Visible','falkon_textdomain') . "|false")
            );
            $options[] = array(
                "section" => "dealer_signup_ids_section",
                "id"      => FALKON_SHORTNAME . "_dealer_signup_stage5_page_id",
                "title"   => __( 'Stage 5 - After GoCardLess', 'falkon_textdomain' ),
                "desc"    => __( 'Contact page of site, used in the theme in the header \'View Our UK Locations\'. ', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array//array( __('Visble','falkon_textdomain') . "|true", __('Not Visible','falkon_textdomain') . "|false")
            );
            //Portal Pages
            $options[] = array(
                "section" => "portal_ids_section",
                "id"      => FALKON_SHORTNAME . "_portal_page_id",
                "title"   => __( 'Portal Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "portal_ids_section",
                "id"      => FALKON_SHORTNAME . "_portal_stock_page_id",
                "title"   => __( 'Stock Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "portal_ids_section",
                "id"      => FALKON_SHORTNAME . "_portal_stock_draft_page_id",
                "title"   => __( 'Draft Stock Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "portal_ids_section",
                "id"      => FALKON_SHORTNAME . "_portal_stock_sold_page_id",
                "title"   => __( 'Sold Stock Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "portal_ids_section",
                "id"      => FALKON_SHORTNAME . "_portal_stock_add_page_id",
                "title"   => __( 'Add Stock CAP Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "portal_ids_section",
                "id"      => FALKON_SHORTNAME . "_portal_stock_add_make_page_id",
                "title"   => __( 'Add Stock M&M Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "portal_ids_section",
                "id"      => FALKON_SHORTNAME . "_portal_stock_upload_page_id",
                "title"   => __( 'Upload Stock CSV Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "portal_ids_section",
                "id"      => FALKON_SHORTNAME . "_portal_lead_page_id",
                "title"   => __( 'Lead Management Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "portal_ids_section",
                "id"      => FALKON_SHORTNAME . "_portal_my_cars_page_id",
                "title"   => __( 'My Cars Page (customer)', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "portal_ids_section",
                "id"      => FALKON_SHORTNAME . "_portal_site_users_page_id",
                "title"   => __( 'Site Users Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "portal_ids_section",
                "id"      => FALKON_SHORTNAME . "_portal_profile_overview_page_id",
                "title"   => __( 'Profile Overview Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "portal_ids_section",
                "id"      => FALKON_SHORTNAME . "_portal_profile_page_id",
                "title"   => __( 'Profile Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "portal_ids_section",
                "id"      => FALKON_SHORTNAME . "_portal_profile_password_page_id",
                "title"   => __( 'Profile - Password Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            //Promo IDs
            $options[] = array(
                "section" => "promo_ids_section",
                "id"      => FALKON_SHORTNAME . "_promo_rewards_page_id",
                "title"   => __( 'Rewards Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "promo_ids_section",
                "id"      => FALKON_SHORTNAME . "_promo_coming_soon_page_id",
                "title"   => __( 'Coming Soon Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "promo_ids_section",
                "id"      => FALKON_SHORTNAME . "_promo_promo_ended_page_id",
                "title"   => __( 'Promotion Ended Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "promo_ids_section",
                "id"      => FALKON_SHORTNAME . "_promo_form_page_id",
                "title"   => __( 'Promotion Form Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
//            $options[] = array(
//                "section" => "promo_ids_section",
//                "id"      => FALKON_SHORTNAME . "_promo_music_page_id",
//                "title"   => __( 'Promotion Music Player Page', 'falkon_textdomain' ),
//                "desc"    => __( '', 'falkon_textdomain' ),
//                "type"    => "select2",
//                "std"    => "",
//                "choices" => $page_array,
//            );
            $options[] = array(
                "section" => "promo_ids_section",
                "id"      => FALKON_SHORTNAME . "_promo_win_lose_page_id",
                "title"   => __( 'Promotion Win/Lose Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "promo_ids_section",
                "id"      => FALKON_SHORTNAME . "_promo_mopup_page_id",
                "title"   => __( 'Mop Up Form Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "promo_ids_section",
                "id"      => FALKON_SHORTNAME . "_promo_mopup_thanks_page_id",
                "title"   => __( 'Mop Up Thank You Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );

            //Promo IDs
            $options[] = array(
                "section" => "loginregister_ids_section",
                "id"      => FALKON_SHORTNAME . "_login_page_id",
                "title"   => __( '_login_page_id', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "loginregister_ids_section",
                "id"      => FALKON_SHORTNAME . "_logout_page_id",
                "title"   => __( '_logout_page_id', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "loginregister_ids_section",
                "id"      => FALKON_SHORTNAME . "_register_club_page_id",
                "title"   => __( '_register_club_page_id', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "loginregister_ids_section",
                "id"      => FALKON_SHORTNAME . "_lost_password_page_id",
                "title"   => __( '_lost_password_page_id', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "loginregister_ids_section",
                "id"      => FALKON_SHORTNAME . "_register_redirect_page_id",
                "title"   => __( '_register_redirect_page_id', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
//            $options[] = array(
//                "section" => "loginregister_ids_section",
//                "id"      => FALKON_SHORTNAME . "_profile_page_id",
//                "title"   => __( '_profile_page_id', 'falkon_textdomain' ),
//                "desc"    => __( '', 'falkon_textdomain' ),
//                "type"    => "select2",
//                "std"    => "",
//                "choices" => $page_array,
//            );


            //Dashboard IDs
            $options[] = array(
                "section" => "dashboard_ids_section",
                "id"      => FALKON_SHORTNAME . "_dashboard_page_id",
                "title"   => __( 'Dashboard Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "dashboard_ids_section",
                "id"      => FALKON_SHORTNAME . "_dashboard_order_page_id",
                "title"   => __( 'Dashboard Order Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
//            $options[] = array(
//                "section" => "dashboard_ids_section",
//                "id"      => FALKON_SHORTNAME . "_portal_profile_overview_page_id",
//                "title"   => __( 'Profile Overview Page', 'falkon_textdomain' ),
//                "desc"    => __( '', 'falkon_textdomain' ),
//                "type"    => "select2",
//                "std"    => "",
//                "choices" => $page_array,
//            );
            $options[] = array(
                "section" => "dashboard_ids_section",
                "id"      => FALKON_SHORTNAME . "_profile_page_id",
                "title"   => __( 'Profile Page', 'falkon_textdomain' ),
                "desc"    => __( 'Page of user\'s profile page. Will be used in non-editable content which links to that page.', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "dashboard_ids_section",
                "id"      => FALKON_SHORTNAME . "_portal_profile_password_page_id",
                "title"   => __( 'Profile - Password Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );

            //General IDs
            $options[] = array(
                "section" => "general_ids_section",
                "id"      => FALKON_SHORTNAME . "_terms_page_id",
                "title"   => __( 'Terms & Conditions page', 'falkon_textdomain' ),
                "desc"    => __( 'Page of site Terms & Conditions. Will be used in non-editable content which links to that page.', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "general_ids_section",
                "id"      => FALKON_SHORTNAME . "_faqs_page_id",
                "title"   => __( 'FAQs page', 'falkon_textdomain' ),
                "desc"    => __( 'Page of site Terms & Conditions. Will be used in non-editable content which links to that page.', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "general_ids_section",
                "id"      => FALKON_SHORTNAME . "_terms_use_page_id",
                "title"   => __( 'Terms of Use page', 'falkon_textdomain' ),
                "desc"    => __( 'Page of site Terms & Conditions. Will be used in non-editable content which links to that page.', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "general_ids_section",
                "id"      => FALKON_SHORTNAME . "_privacy_page_id",
                "title"   => __( 'Privacy Policy page', 'falkon_textdomain' ),
                "desc"    => __( 'Page of site Terms & Conditions. Will be used in non-editable content which links to that page.', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            break;
		
		case 'social-media':
            $options[] = array(
                "section" => "social_footer_section",
                "id"      => FALKON_SHORTNAME . "_multicheckbox_inputs",
                "title"   => __( 'Multi-Checkbox', 'wptuts_textdomain' ),
                "desc"    => __( 'You can only choose <strong>5</strong> social links, choose wisely.', 'wptuts_textdomain' ),
                "type"    => "multi-checkbox-limit",	//variation of the std multi-checkbox, with a limit of allowed ticks
                "std"     => 5,	//the limit of allowed ticks
                "choices" => array( __('Facebook','wptuts_textdomain') . "|".FALKON_SHORTNAME . "_url_fb",
                    __('Twitter','wptuts_textdomain') . "|".FALKON_SHORTNAME . "_url_twitter",
                    __('YouTube','wptuts_textdomain') . "|".FALKON_SHORTNAME . "_url_youtube",
                    __('LinkedIn','wptuts_textdomain') . "|".FALKON_SHORTNAME . "_url_linkedin",
                    __('Instagram','wptuts_textdomain') . "|".FALKON_SHORTNAME . "_url_inst",
                    __('Pinterest','wptuts_textdomain') . "|".FALKON_SHORTNAME . "_url_pin",
                    __('Google+','wptuts_textdomain') . "|".FALKON_SHORTNAME . "_url_google_plus",
                    __('Vimeo','wptuts_textdomain') . "|".FALKON_SHORTNAME . "_url_vimeo",
                    __('Tumblr','wptuts_textdomain') . "|".FALKON_SHORTNAME . "_url_tumblr",
                )
            );
			$options[] = array(
				"section" => "social_footer_section",
				"id"      => FALKON_SHORTNAME . "_url_fb",
				"title"   => __( 'Facebook URL', 'falkon_textdomain' ),
				//"desc"    => __( 'A text input field which can be used for urls.', 'falkon_textdomain' ),
				"type"    => "text",
				//"std"     => "",
				"class"   => "url"
			);
			$options[] = array(
				"section" => "social_footer_section",
				"id"      => FALKON_SHORTNAME . "_url_twitter",
				"title"   => __( 'Twitter URL', 'falkon_textdomain' ),
				//"desc"    => __( 'A text input field which can be used for urls.', 'falkon_textdomain' ),
				"type"    => "text",
				//"std"     => "http://wp.tutsplus.com",
				"class"   => "url"
			);
			$options[] = array(
				"section" => "social_footer_section",
				"id"      => FALKON_SHORTNAME . "_url_youtube",
				"title"   => __( 'YouTube URL', 'falkon_textdomain' ),
				//"desc"    => __( 'A text input field which can be used for urls.', 'falkon_textdomain' ),
				"type"    => "text",
				//"std"     => "http://wp.tutsplus.com",
				"class"   => "url"
			);
			$options[] = array(
				"section" => "social_footer_section",
				"id"      => FALKON_SHORTNAME . "_url_linkedin",
				"title"   => __( 'LinkedIn URL', 'falkon_textdomain' ),
				//"desc"    => __( 'A text input field which can be used for urls.', 'falkon_textdomain' ),
				"type"    => "text",
				//"std"     => "http://wp.tutsplus.com",
				"class"   => "url"
			);
			$options[] = array(
				"section" => "social_footer_section",
				"id"      => FALKON_SHORTNAME . "_url_inst",
				"title"   => __( 'Instagram URL', 'falkon_textdomain' ),
				//"desc"    => __( 'A text input field which can be used for urls.', 'falkon_textdomain' ),
				"type"    => "text",
				//"std"     => "http://wp.tutsplus.com",
				"class"   => "url"
			);
			$options[] = array(
				"section" => "social_footer_section",
				"id"      => FALKON_SHORTNAME . "_url_pin",
				"title"   => __( 'Pinterest URL', 'falkon_textdomain' ),
				//"desc"    => __( 'A text input field which can be used for urls.', 'falkon_textdomain' ),
				"type"    => "text",
				//"std"     => "http://wp.tutsplus.com",
				"class"   => "url"
			);
			$options[] = array(
				"section" => "social_footer_section",
				"id"      => FALKON_SHORTNAME . "_url_google_plus",
				"title"   => __( 'Google+ URL', 'falkon_textdomain' ),
				//"desc"    => __( 'A text input field which can be used for urls.', 'falkon_textdomain' ),
				"type"    => "text",
				//"std"     => "http://wp.tutsplus.com",
				"class"   => "url"
			);
            $options[] = array(
                "section" => "social_footer_section",
                "id"      => FALKON_SHORTNAME . "_url_vimeo",
                "title"   => __( 'Vimeo URL', 'falkon_textdomain' ),
                //"desc"    => __( 'A text input field which can be used for urls.', 'falkon_textdomain' ),
                "type"    => "text",
                //"std"     => "http://wp.tutsplus.com",
                "class"   => "url"
            );
            $options[] = array(
                "section" => "social_footer_section",
                "id"      => FALKON_SHORTNAME . "_url_tumblr",
                "title"   => __( 'Tumblr URL', 'falkon_textdomain' ),
                //"desc"    => __( 'A text input field which can be used for urls.', 'falkon_textdomain' ),
                "type"    => "text",
                //"std"     => "http://wp.tutsplus.com",
                "class"   => "url"
            );
		break;

		case 'contacts':


            $pagepost_list = get_pages();
            $page_array = array();
            array_push($page_array,'Please Select|');
            foreach ( $pagepost_list as $page ) {
                $page_ancestors = get_post_ancestors( $page->ID );
                $level_deep = count($page_ancestors);
                $delim = str_repeat("— ", ($level_deep));
                if($page->post_parent!=0) $page->post_title = $delim.' '.$page->post_title;
                array_push($page_array,$page->post_title.'|'.$page->ID);
            }
            $options[] = array(
                "section" => "contact_section",
                "id"      => FALKON_SHORTNAME . "_contact_page_id",
                "title"   => __( 'Contact Us page', 'falkon_textdomain' ),
                "desc"    => __( 'Contact page of site, used in the backend for now. ', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array//array( __('Visble','falkon_textdomain') . "|true", __('Not Visible','falkon_textdomain') . "|false")
            );



            break;
        case 'smtp_email':
            //Email sending settings
            $options[] = array(
                "section" => "email_section",
                "id"      => FALKON_SHORTNAME . "_email_from_name",
                "title"   => __( 'From Name:', 'falkon_textdomain' ),
                "desc"    => __( 'Enter a name that will appear in the "From" field in a users\' email client.', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "text"
            );
            $options[] = array(
                "section" => "email_section",
                "id"      => FALKON_SHORTNAME . "_email_from_address",
                "title"   => __( 'From Email Address:', 'falkon_textdomain' ),
                "desc"    => __( 'Enter a from email address that will appear in the "From" field in a users\' email client.', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "text"
            );
            $options[] = array(
                "section" => "email_section",
                "id"      => FALKON_SHORTNAME . "_email_reply_address",
                "title"   => __( 'Reply To Email Address:', 'falkon_textdomain' ),
                "desc"    => __( 'Enter a reply to email address that will appear in the "Reply To" field in a users\' email client.', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "text"
            );
            $options[] = array(
                "section" => "email_section",
                "id"      => FALKON_SHORTNAME . "_email_bcc_admin",
                "title"   => __( 'Admin Bcc Emails:', 'falkon_textdomain' ),
                "desc"    => __( 'Add a comma-separated list of admin emails addresses which will be added to all automated emails in the Bcc field. (Is not used in Contact From 7 - Use individual form settings)', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "text widefat"
            );
            //SMTP Email settings
            $options[] = array(
                "section" => "smtp_email_section",
                "id"      => FALKON_SHORTNAME . "_email_smtp_enable",
                "title"   => __( 'Enable SMTP:', 'falkondigital' ),
                "desc"    => __( 'Enable emails to be sent via SMTP?', 'falkon_textdomain' ),
                "type"    => "radio",
                "std"       =>  "1",
                'choices' => array(
                    'Enabled|1',
                    'Disabled|0',
                )
            );
            $options[] = array(
                "section" => "smtp_email_section",
                "id"      => FALKON_SHORTNAME . "_email_smtp_host",
                "title"   => __( 'SMTP Host:', 'falkon_textdomain' ),
                "desc"    => __( 'Add the SMTP host IP or name here.', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "text regular-text code"
            );

            $options[] = array(
                "section" => "smtp_email_section",
                "id"      => FALKON_SHORTNAME . "_email_smtp_auth",
                "title"   => __( 'SMTP Authentication:', 'falkon_textdomain' ),
                "desc"    => __( 'Add the SMTP host IP or name here.', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "false",
                "choices" => array( __('True','falkon_textdomain') . "|true", __('False','falkon_textdomain') . "|false")
            );
            $options[] = array(
                "section" => "smtp_email_section",
                "id"      => FALKON_SHORTNAME . "_email_smtp_port",
                "title"   => __( 'SMTP Port Number:', 'falkon_textdomain' ),
                "desc"    => __( 'SMTP port number - likely to be 25, 465 or 587', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "25",
                "class"   => "numeric regular-text code"
            );
            $options[] = array(
                "section" => "smtp_email_section",
                "id"      => FALKON_SHORTNAME . "_email_smtp_user",
                "title"   => __( 'SMTP username:', 'falkon_textdomain' ),
                "desc"    => __( 'Username to use for SMTP authentication.', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "text"
            );
            $options[] = array(
                "section" => "smtp_email_section",
                "id"      => FALKON_SHORTNAME . "_email_smtp_pass",
                "title"   => __( 'SMTP password:', 'falkon_textdomain' ),
                "desc"    => __( 'Password to use for SMTP authentication.', 'falkon_textdomain' ),
                "type"    => "password",
                "std"     => "",
                "class"   => "password"
            );
            $options[] = array(
                "section" => "smtp_email_section",
                "id"      => FALKON_SHORTNAME . "_email_smtp_secure",
                "title"   => __( 'SMTP Security Used:', 'falkon_textdomain' ),
                "desc"    => __( 'The encryption system to use - nothing, ssl (deprecated) or tls.', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "tls",
                "choices" => array( __('Nothing','falkon_textdomain') . "|", __('SSL','falkon_textdomain') . "|ssl", __('TLS','falkon_textdomain') . "|tls")
            );
            break;

        case 'homepage-blocks':

            break;
		// Textareas
		case 'homepage-blocks':
			$options[] = array(
				"section" => "xhomepageblocks_section",
				"id"      => FALKON_SHORTNAME . "_block_visible",
				"title"   => __( 'Homepage Blocks Visible?', 'falkon_textdomain' ),
				"desc"    => __( 'Make the block of three homepage items visible on the front end?', 'falkon_textdomain' ),
				"type"    => "select2",
				"std"    => "true",
				"choices" => array( __('Visble','falkon_textdomain') . "|true", __('Not Visible','falkon_textdomain') . "|false")
			);

		break;
		
		

	}
	
	return $options;	
}

/**
 * Contextual Help
 */
function falkon_options_two_page_contextual_help() {
	
	// get the current tab
	$tab = falkon_get_the_tab();
	
	$text 	= "<h3>" . __('Falkon Digital Theme - Contextual Help','falkon_textdomain') . "</h3>";
	
	// contextual help according to tab
	switch ($tab) {
		// Text Inputs
		case 'general':
			$text 	.= "<p>" . __('General Settings Page help.','falkon_textdomain') . "</p>";
		break;
        case 'hoemage':
            $text 	.= "<p>" . __('Homepage Settings Page help.','falkon_textdomain') . "</p>";
            break;
		// Textareas
		case 'textareas':
			$text 	.= "<p>" . __('Contextual help for the "Textarea" settings fields goes here.','falkon_textdomain') . "</p>";
		break;
		
		// Select
		case 'select':
			$text 	.= "<p>" . __('Contextual help for the "Select" settings fields goes here.','falkon_textdomain') . "</p>";
		break;
		
		// Checkboxes
		case 'checkboxes':
			$text 	.= "<p>" . __('Contextual help for the "Checkboxes" settings fields goes here.','falkon_textdomain') . "</p>";
		break;
	}
	
	// must return text! NOT echo
	return $text;
} ?>