<?php
/**
 *
 */
?>
<?php

global $our_work_mb;
$related_meta_items = $our_work_mb->the_meta();

$currentId = $post->ID;
$related_items = isset($related_meta_items['related_items'])?(array)$related_meta_items['related_items']:array();
$custom_post = 'our-work';
$custom_post_object = get_post_type_object( $custom_post );
$custom_post_name = $custom_post_object->labels->name;
$xyz=0;
$number_of_related_posts = $falkon_option['falkon_related_motorhome_num']? $falkon_option['falkon_related_motorhome_num'] : 3;

//var_dump($falkon_option);
//var_dump($number_of_related_posts);

$exisiting_posts = array();
$exisiting_posts[] = $currentId;
$exisiting_posts = array_merge($related_items,$exisiting_posts);

if(count($related_items)<$number_of_related_posts){
	$args = array(
		'posts_per_page'    =>	($number_of_related_posts-count($related_items)),
		'post_type'         =>  $custom_post,
		'post_status'	    =>  'publish',
		'post__not_in'      =>  $exisiting_posts,
		'orderby'		    => 'rand',
		'fields'            =>  'ids'
	);
	$extra_posts = new WP_Query( $args );
	if($extra_posts->have_posts()) {
		$related_items = array_merge($related_items,$extra_posts->posts);
		wp_reset_postdata();
	}
}

$args = array(
	'post_type'         =>  $custom_post,
	'post_status'	    =>  'publish',
	'post__in'          =>  $related_items,
	'orderby'		    => 'post__in',
	'posts_per_page'    =>	$number_of_related_posts,
);
$related_query = new WP_Query( $args );

echo '<div class="container">';
if($related_query->have_posts()){
	$related_posts_title = $falkon_option['falkon_related_title_text']? $falkon_option['falkon_related_title_text'] : 'Related stuff here';
	echo '<h3 class="text-center">'.$related_posts_title.'</h3><br/><br/>';
	while ( $related_query->have_posts() ) :
		$related_query->the_post();
		//THE RELATED LOOP, do your stuff here...

		echo '<a href="'. get_the_permalink().'">';
		echo '<div class="col-xs-12 col-sm-4"><div class="rel-feat-img">'.get_the_post_thumbnail('','full', array('class'=>'child-img'));
		echo '<span class="hidden-xs imgcattag">'. get_the_title().'</span></a>';
	    echo '</div></div>';


	endwhile;
	wp_reset_postdata();
}
echo '</div>';