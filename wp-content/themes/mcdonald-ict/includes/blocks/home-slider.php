<?php
$args = array(
    'orderby' => 'menu_order post_date',
    'order' => 'ASC',
    'post_type' => 'slide',
    'post_status' => 'publish',
    'posts_per_page' => -1,
);
$recent_posts = new WP_Query( $args );
$slide_num = 0;
if ( $recent_posts->have_posts() ) :
    $slide_num = $recent_posts->post_count;
    ?>
    <div id="home-page-slider" class="owl-carousel">
        <?php /* ?><ul  class="homepage-slider"><?php */ ?>
        <?php
        while ( $recent_posts->have_posts() ) : $recent_posts->the_post();
            global $slide_mb;
            $slide_meta = $slide_mb->the_meta();
//            var_dump($slide_meta);
            if($slide_meta['image_id']){

                $image_attributes = wp_get_attachment_image_src( $slide_meta['image_id'], 'slide-image-full');
                $img_url = $image_attributes[0];
                $img_src = '<img src="'.$img_url.'" class="slideimg">';
            }
            else{
                $img_url = get_template_directory_uri().'/images/default-slider.jpg';
                $img_src = '<img src="'.$img_url.'" class="slideimg default-image">';
            }

            //bg
            $result = '<div style="background-image:url('.$img_url.');" class="banner-bg"/>';
            $result .= '<div class="banner-content">';
            $result .= '<div class="container">';
               $result .= '<div class="col-xs-12 col-sm-offset-1 col-sm-10 text-center">';
                    $result .= $slide_meta['title_line']? '<h2>'.$slide_meta['title_line'].'</h2>':'';
                    $result .= $slide_meta['tag_line']? '<span>'.$slide_meta['tag_line'].'</span>':'';
                    $result .= '<br><br>';
                    $result .= '<a class="btn button btn-bner" href="'.$slide_meta['link'].'">';
                    $result .= $slide_meta['black_box_text']? $slide_meta['black_box_text'] :'';
                    $result .= '</a>';
                    $result .= $slide_meta['findoutmore_link']? '<a href="'.$slide_meta['findoutmore_link'].'">':'';
                    $result .= '</a>';
              $result .= '</div>';
            $result .= '</div>';
            $result .= '</div>';
            // bg
            $result .= '</div>';

            echo $result;





        endwhile;
        wp_reset_postdata();
        ?>
    </div>



    <?php /* ?></ul><?php */ ?>
    <?php
    $carousel_loop = $slide_num>1?'true':'false';
    $mouse_drag = $slide_num>1?'true':'false';
    $touch_drag = $slide_num>1?'true':'false';
    ?>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            var owlSlider = $('#home-page-slider');
            owlSlider.owlCarousel({
                slideSpeed : 800,
                paginationSpeed : 500,
                singleItem:true,
                autoPlay : true,
                autoplayHoverPause: true,
                loop: <?php echo $carousel_loop;?>,
                mouseDrag:<?php echo $mouse_drag;?>,
                touchDrag:<?php echo $touch_drag;?>
            });

            $('.play').on('click', function () {
                owlSlider.trigger('play.owl.autoplay', [1000])
            })

            $('.stop').on('click', function () {
                owlSlider.trigger('stop.owl.autoplay')
            })
        });
    </script>
    <?php
endif;
?>
