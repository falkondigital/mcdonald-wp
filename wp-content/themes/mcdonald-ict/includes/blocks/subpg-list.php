<?php

$base_args = array(
    'hierarchical' => 0
);
if (has_children()) {
    $args = array(
        'child_of' => $post->ID,
        'parent' => $post->ID
    );

    echo '<section id="subpage-list" class="hidden-xs">';
    echo '<div class="container text-center">';
    echo '<ul>';
    $args = array_merge($base_args, $args);

    $pages = get_pages($args);

    foreach ($pages as $page) {
        $current_page = $page->ID;
        if (get_the_id()==$current_page) {
            echo '<li class="active-pg">';
        }
        else  {
            echo'<li>';
        }
        echo '<a href="' . get_permalink($page->ID) . '">' . $page->post_title . '</a></li>';
    }

    echo '</ul>';
    echo '</section>';
    echo '</div>';

}

elseif (is_top_level()) {
        $args = array(
            'child_of' => $post->post_parent,
            'parent' => $post->post_parent
        );

    echo '<section id="subpage-list" class="hidden-xs">';
    echo '<div class="container text-center">';
    echo '<ul>';
    $args = array_merge($base_args, $args);

    $pages = get_pages($args);

    foreach ($pages as $page) {
        $current_page = $page->ID;
        if (get_the_id()==$current_page) {
            echo '<li class="active-pg">';
        }
        else  {
            echo'<li>';
        }
        echo '<a href="' . get_permalink($page->ID) . '">' . $page->post_title . '</a></li>';
    }

    echo '</ul>';
    echo '</section>';
    echo '</div>';

}
  else {
    $args = array(
        'parent' => 0
    );
      echo '';
}



