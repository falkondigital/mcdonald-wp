<?php global $falkon_option; ?>
<!DOCTYPE html>
<html class="no-js">
<head>
	<title><?php wp_title('•', true, 'right'); bloginfo('name'); ?></title>

    <?php include(locate_template('includes/blocks/google-analytics.php'));?>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

<!--[if lt IE 8]>
<div class="alert alert-warning">
	You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
</div>
<![endif]-->


<header>
	<div id="header">


		<div class="visible-xs visible-sm visible-md hidden-lg">

			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu">

				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar top-bar"></span>
				<span class="icon-bar middle-bar"></span>
				<span class="icon-bar bottom-bar"></span>

			</button>


			<div class="collapse" id="menu">

				<?php
				wp_nav_menu( array(
						'theme_location'    => 'navbar-main',
						'depth'             => 2,
						'menu_class'        => '',
						'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
						'walker'            => new wp_bootstrap_navwalker())
				);
				?>
				<ul style="padding-top:0px; padding-right:40px;" class="text-right">
					<li>
				      <?php if($falkon_option['falkon_contact_email']) echo '<i style="color:#fff;" class="fa fa-envelope" aria-hidden="true"></i> <a style="padding-left:5px;" href="mailto:'.$falkon_option['falkon_contact_email'].'">Email Us</a><br>'; ?>
					</li>
				</ul>
			</div>

		</div>




		<div class="container">

			<div class="col-xs-12 col-sm-12 col-md-12" >
				<nav class="navbar navbar-default">


						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name','display' ).( get_bloginfo('description')!=''? ' - '.get_bloginfo('description'): '')); ?>" rel="nofollow home" class="navbar-brand">
							<img src="<?php echo get_stylesheet_directory_uri();?>/images/logo.png" class="site-logo">
						</a>

					<div class="collapse navbar-collapse" id="navbar">
						<div class="header-ctd navbar-right">

                            <?php if($falkon_option['falkon_contact_number']) echo '<i class="fa fa-phone" aria-hidden="true"></i> <a href="tel:'.$falkon_option['falkon_contact_number'].'"><span class="number">'.$falkon_option['falkon_contact_number'].'</span></a>'; ?>&nbsp;&nbsp;
                            <?php if($falkon_option['falkon_contact_email']) echo '<i style="color:#0771b8;" class="fa fa-envelope" aria-hidden="true"></i> <a href="mailto:'.$falkon_option['falkon_contact_email'].'"><span class="email">'.$falkon_option['falkon_contact_email'].'</span></a>'; ?>

                        </div>
						<?php
							wp_nav_menu( array(
								'theme_location'    => 'navbar-main',
								'depth'             => 2,
								'menu_class'        => 'nav navbar-nav navbar-right',
								'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
								'walker'            => new wp_bootstrap_navwalker())
							);
						?>
						<?php // get_template_part('includes/navbar-search'); ?>

					</div><!-- /.navbar-collapse -->

			</div><!-- /.navbar-collapse -->




		</div>

	</div><!-- // #header -->

</header>