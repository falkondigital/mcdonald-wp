<?php
// Register Custom Post Type 'Testimonial'
function reg_post_testimonial() {
	$labels = array(
		'name'                => _x( 'Testimonials', 'Post Type General Name', 'falkondigital' ),
		'singular_name'       => _x( 'Our Testimonials', 'Post Type Singular Name', 'falkondigital' ),
		'menu_name'           => __( 'Testimonials', 'falkondigital' ),
		'parent_item_colon'   => __( 'Parent Testimonial', 'falkondigital' ),
		'all_items'           => __( 'All Testimonials', 'falkondigital' ),
		'view_item'           => __( 'View Testimonial', 'falkondigital' ),
		'add_new_item'        => __( 'Add New Testimonial', 'falkondigital' ),
		'add_new'             => __( 'New Testimonial', 'falkondigital' ),
		'edit_item'           => __( 'Edit Testimonial', 'falkondigital' ),
		'update_item'         => __( 'Update Testimonial', 'falkondigital' ),
		'search_items'        => __( 'Search properties', 'falkondigital' ),
		'not_found'           => __( 'No properties found', 'falkondigital' ),
		'not_found_in_trash'  => __( 'No properties found in Trash', 'falkondigital' ),
	);

	$args = array(
		'label'               => __( 'testimonial', 'falkondigital' ),
		'description'         => __( 'Testimonials', 'falkondigital' ),
		'labels'              => $labels,
		'supports'            => array('' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'menu_position'       => 28,
		'menu_icon'             =>'dashicons-format-quote',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => true,
		'publicly_queryable'  => false,
		'capability_type'     => 'page',
		'rewrite'            => array( 'slug' => 'testimonial','with_front' => false ),
	);

	register_post_type( 'testimonial', $args );
}

// Hook into the 'init' action
add_action( 'init', 'reg_post_testimonial', 0 );


/*
 * Add custom messages for the three custom post types
 */
function updated_messages_testimonial( $messages ) {
	global $post, $post_ID;
	$messages['testimonial'] = array(
		0 => '',	// Unused. Messages start at index 1.
		1 => __('Testimonial updated.'),
		2 => __('Custom field updated.'),
		3 => __('Custom field deleted.'),
		4 => __('Testimonial updated.'),
		5 => isset($_GET['revision']) ? sprintf( __('Testimonial restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => __('Testimonial published.'),
		7 => __('Testimonial saved.'),
		8 => sprintf( __('Testimonial submitted. <a target="_blank" href="%s">Preview car?</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
		9 => sprintf( __('Testimonial scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview car?</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
		10 => sprintf( __('Testimonial draft updated. <a target="_blank" href="%s">Preview car?</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
	);
	return $messages;
}
add_filter( 'post_updated_messages', 'updated_messages_testimonial' );



function myactivationfunction_testimonial() {
	//my_custom_post_product();
 	flush_rewrite_rules();
}
add_action("after_switch_theme", "myactivationfunction_testimonial", 10 ,  2);
function mydeactivationfunction_testimonial() {
 	flush_rewrite_rules();
}
add_action("switch_theme", "mydeactivationfunction_testimonial", 10 ,  2);


include_once get_template_directory() . '/metaboxes/wpalchemy/metabox.php';

include_once get_template_directory() . '/metaboxes/testimonial-spec.php';

//include_once get_template_directory() . '/metaboxes/testimonial-image-spec.php';



add_filter( 'manage_edit-testimonial_columns', 'testimonial_columns_filter', 10, 1 );

function testimonial_columns_filter( $columns ) {

 	//$column_thumbnail = array( 'prop-thumb' => 'Image' );
//	$column_thumbnail = array( 'testimonial-thumb' => 'Thumbnail' );

	$column_propstatus = array( 'testimonial-info' => 'Information' );
	$column_desc = array( 'testimonial-desc' => 'Description' );

	$column_featured = array( 'featured' => 'Featured' );

	//$columns = array_slice( $columns, 0, 1, true ) + $column_thumbnail + array_slice( $columns, 1, NULL, true );
//	$columns = array_slice( $columns, 0, 1, true ) + $column_thumbnail + array_slice( $columns, 1, NULL, true );
	$columns = array_slice( $columns, 0, 3, true ) + $column_desc + array_slice( $columns, 3, NULL, true );
	$columns = array_slice( $columns, 0, 4, true ) + $column_featured + array_slice( $columns, 4, NULL, true );

	$columns = array_slice( $columns, 0, 2, true ) + $column_propstatus + array_slice( $columns,2, NULL, true );

	//$columns = array_slice( $columns, 0, 8, true ) + $column_propfeat + array_slice( $columns, 8, NULL, true );
	unset($columns['title']);
	unset($columns['author']);
	unset($columns['date']);
	unset($columns['comments']);
	return $columns;

}

add_action( 'manage_posts_custom_column', 'my_column_action_testimonial', 10, 1 );

function my_column_action_testimonial( $column ) {

	global $post;

	switch ( $column ) {
		case 'testimonial-thumb':
			global $testimonial_image_mb;
			$testimonial_meta = $testimonial_image_mb->the_meta();
			//var_dump($slide_meta);
			if(isset($testimonial_meta['image_id'])){

				$image_attributes = wp_get_attachment_image_src( $testimonial_meta['image_id'], 'slide-image-thumb');
				$img_url = $image_attributes[0];
				$img_src = '<img src="'.$img_url.'" width="200" height="auto" class="slideimg">';
			}
			else{

				$img_url = get_template_directory_uri().'/images/default-thumbnail.jpg';
				$img_src = '<img src="'.$img_url.'" width="200" height="auto" class="slideimg">';

			}
			//echo '<a href="'.admin_url('post.php?post='.get_the_id().'&action=edit').'">'.$img_src.'</a>';
			echo '<a href="'.admin_url('post.php?post='.get_the_id().'&action=edit').'" title="Edit this slide">'
			?>
			<div id="slide-preview" class="list-preview">
				<?php echo $img_src;?>
				<?php if($slide_meta['title_line']!='' or $slide_meta['tag_line']!=''){ ?>
					<div class="banner-tag">
						<div class="banner-title"><?php echo $slide_meta['title_line'];?></div>
						<div class="banner-text"><?php echo $slide_meta['tag_line'];?></div>
					</div>
				<?php } ?>
			</div>
			<?php
			echo '</a>';
			echo '<a href="'.admin_url('post.php?post='.get_the_id().'&action=edit').'" title="Edit this slide">Edit</a> | ';
			echo '<a class="submitdelete" title="Move this slide to the Trash"
            href="'.get_delete_post_link(get_the_id()).'">Trash</a>';

			break;
		case 'testimonial-desc':
			global $testimonial_mb;
			$testimonial_meta = $testimonial_mb->the_meta();
			echo wp_trim_words($testimonial_meta['description'],20,'...');
			break;
		case 'testimonial-info':
			global $testimonial_mb;
			$testimonial_meta = $testimonial_mb->the_meta();
			echo '<strong>Name:</strong> '.$testimonial_meta['name'].'<br />';
			echo '<strong>Position:</strong> '.$testimonial_meta['position'].'<br />';
			echo '<strong>Company:</strong> '.$testimonial_meta['company'].'<br />';
			echo '<a href="'.admin_url('post.php?post='.get_the_id().'&action=edit').'" title="Edit this">Edit This</a>';
		break;

	}
}