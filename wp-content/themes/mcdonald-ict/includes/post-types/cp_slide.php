<?php

// Register Custom Post Type
function create_cp_slide() {

	$labels = array(
		'name'                => _x( 'Slides', 'Post Type General Name', 'admedsol' ),
		'singular_name'       => _x( 'Slide', 'Post Type Singular Name', 'admedsol' ),
		'menu_name'           => __( 'Homepage Slider', 'admedsol' ),
		'parent_item_colon'   => __( 'Parent Slide:', 'admedsol' ),
		'all_items'           => __( 'All Slides', 'admedsol' ),
		'view_item'           => __( 'View Slide', 'admedsol' ),
		'add_new_item'        => __( 'Add New Slide', 'admedsol' ),
		'add_new'             => __( 'Add Slide', 'admedsol' ),
		'edit_item'           => __( 'Edit Slide', 'admedsol' ),
		'update_item'         => __( 'Update Slide', 'admedsol' ),
		'search_items'        => __( 'Search Slide', 'admedsol' ),
		'not_found'           => __( 'No slides found, why not create one?', 'admedsol' ),
		'not_found_in_trash'  => __( 'Slide not found in Trash', 'admedsol' ),
	);
	$args = array(
		'label'               => __( 'slide', 'admedsol' ),
		'description'         => __( 'Slides registered with site', 'admedsol' ),
		'labels'              => $labels,
		'supports'            => array('' ),
		'hierarchical'        => false,
		'public'              => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
        'menu_icon'           => 'dashicons-images-alt2',
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => true,
		'publicly_queryable'  => false,
		'capability_type'     => 'post',
	);
	register_post_type( 'slide', $args );

    add_image_size('slide-image-full',1920,600,true);
    add_image_size('slide-image-thumb',320,100,true);

    add_filter('image_size_names_choose', 'my_image_sizes');
    function my_image_sizes($sizes) {
        $addsizes = array(
            "slide-image-thumb" => __( "Slide Banner Thumb")
        );
        $newsizes = array_merge($sizes, $addsizes);
        return $newsizes;
    }
}

// Hook into the 'init' action
add_action( 'init', 'create_cp_slide', 0 );

//Load WP Alchemy
//include_once get_template_directory() . '/metaboxes/setup.php';
//Main Slide Spec
include_once get_template_directory() . '/metaboxes/slide-spec.php';

function change_default_title_slide( $title ){
     $screen = get_current_screen();
     if  ( 'slide' == $screen->post_type ) {
          $title = 'Enter title of the slide here, it will not be visible on the front end';
     }
     return $title;
}
add_filter( 'enter_title_here', 'change_default_title_slide' );

add_filter( 'manage_slide_posts_columns', 'slide_columns_filter', 10, 1 );
function slide_columns_filter( $columns ) {
 	$column_thumbnail = array( 'slide-thumb' => 'Preview' );
	$column_reviews = array( 'slide-reviews' => 'Details' );

	$columns = array_slice( $columns, 0, 1, true ) + $column_thumbnail + array_slice( $columns, 1, NULL, true );
	$columns = array_slice( $columns, 0, 3, true ) + $column_reviews + array_slice( $columns, 3, NULL, true );

    unset($columns['title']);
	unset($columns['author']);
	unset($columns['date']);
	unset($columns['comments']);
	return $columns;
}
add_action( 'manage_slide_posts_custom_column', 'slide_column_action', 10, 1 );
function slide_column_action( $column ) {
	global $post;
	switch ( $column ) {
		case 'slide-thumb':
            global $slide_mb;
            $slide_meta = $slide_mb->the_meta();
			if($slide_meta['image_id']){
				$image_attributes = wp_get_attachment_image_src( $slide_meta['image_id'], 'slide-image-thumb');
				$img_url = $image_attributes[0];
                $img_src = '<img src="'.$img_url.'" width="200" height="auto" class="slideimg">';
			}
			else{
					$img_url = get_template_directory_uri().'/images/default-slider-thumbnail.jpg';
                $img_src = '<img src="'.$img_url.'" width="200" height="auto" class="slideimg">';

			}	
            echo '<a href="'.admin_url('post.php?post='.get_the_id().'&action=edit').'" title="Edit this slide">'
            ?>
<div id="slide-preview" class="list-preview">
        <?php echo $img_src;?>
<?php if($slide_meta['title_line']!='' or $slide_meta['tag_line']!=''){ ?>
    <div class="slide-tag <?php echo 'tagbox-'.$slide_meta['box_side'];?><?php echo ($slide_meta['sap_blue']=='1'?' sap-blue':'')?>">
        <div class="banner-title"><?php echo $slide_meta['title_line'];?></div>
        <div class="banner-text"><?php echo $slide_meta['tag_line'];?></div>
        <div class="banner-desc"><?php echo $slide_meta['desc'];?></div>
        <?php
        if($slide_meta['button_text']!='' and $slide_meta['link']!=''){
            echo '<div class="btn-slide">'.$slide_meta['button_text'].'</div>';
        }
        ?>
    </div>
<?php } ?>
</div>
<?php
            echo '</a>';
//            echo '<div class="row-actions">';
//            echo '<a href="'.admin_url('post.php?post='.get_the_id().'&action=edit').'" title="Edit this slide">Edit</a> | ';
//            echo '<a class="submitdelete" title="Move this slide to the Trash"
//            href="'.get_delete_post_link(get_the_id()).'">Trash</a>';
//            echo '</div>';

		break;
		case 'slide-reviews':
            $slide_id = (int)$post->ID;
            global $slide_mb;
            $slide_meta = $slide_mb->the_meta();

            echo '<div style="display: table;">';

            if($post->post_status =='draft'){
                echo '<div style="display: table-row">
                    <div style="display: table-cell; white-space: nowrap; padding-right: 0.75em;"><strong>Status:</strong></div>
                    <div style="display: table-cell"><strong>THIS IS CURRENTLY DRAFT</strong></div>
                </div>';
            }
            echo '<div style="display: table-row">
                    <div style="display: table-cell; white-space: nowrap; padding-right: 0.75em;">Title Line:</div>
                    <div style="display: table-cell">'.($slide_meta['title_line'] ? $slide_meta['title_line'] : '').'</div>
                </div>
                <div style="display: table-row">
                    <div style="display: table-cell; white-space: nowrap; padding-right: 0.75em;" id="tag-text-th">Description Text:</div>
                    <div style="display: table-cell">'.($slide_meta['tag_line'] ? $slide_meta['tag_line'] : '').'</div>
                </div>
                <div style="display: table-row">
                    <div style="display: table-cell; white-space: nowrap; padding-right: 0.75em;" id="tag-text-th">Button Text:</div>
                    <div style="display: table-cell">'.($slide_meta['black_box_text'] ? $slide_meta['black_box_text'] : '').'</div>
                </div>
                <div style="display: table-row">
                    <div style="display: table-cell; white-space: nowrap; padding-right: 0.75em;">Slide Link?:</div>
                    <div style="display: table-cell">'.($slide_meta['link']? '<a href="'.esc_url($slide_meta['link']).'" rel="nofollow" target="_blank" title="Click to go to \''.esc_attr($slide_meta['link']).'\' in a new window">'.$slide_meta['link'].'</a>':'no link').'</div>
                </div>

            </div>';
		break;
	}
}

add_filter('post_updated_messages', 'codex_book_updated_messages');
function codex_book_updated_messages( $messages ) {
    global $post, $post_ID;

    $messages['slide'] = array(
        0 => '', // Unused. Messages start at index 1.
        1 => sprintf( __('Slide updated.'), esc_url( get_permalink($post_ID) ) ),
        2 => __('Custom field updated.'),
        3 => __('Custom field deleted.'),
        4 => __('Slide updated.'),
        /* translators: %s: date and time of the revision */
        5 => isset($_GET['revision']) ? sprintf( __('Slide restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
        6 => sprintf( __('Slide published.'), esc_url( get_permalink($post_ID) ) ),
        7 => __('Slide saved.'),
        8 => sprintf( __('Slide submitted. <a target="_blank" href="%s">Preview slide</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
        9 => sprintf( __('Slide scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview slide</a>'),
            // translators: Publish box date format, see http://php.net/date
            date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
        10 => sprintf( __('Slide draft updated. <a target="_blank" href="%s">Preview slide</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    );

    return $messages;
}
