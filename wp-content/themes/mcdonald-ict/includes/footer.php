<?php
// Falkon Options
 global $falkon_option;

global $footercta_meta;
$footer_meta = $footercta_meta->the_meta();

?>

<?php
if($footer_meta['hide_footer_cta']=='1'){
  }
else  {
    ?>
    <section id="pre-footer">
		   <div class="container">
		        <div class="col-xs-12 col-sm-12 text-center">

    <span>Book a free, no obligation consulation now</span> <div class="visible-xs"><br/></div> <a href="<?php echo esc_url( home_url( '/contact-us' ) ); ?>" class="btn button pre-btn">Book Now</a>

		        </div>
		   </div>
		 </section>
<?php
}
?>
<!-- END footer CTA -->

<section id="partner-logos">
	<div class="container">
<?php
$thumb_array = $falkon_option['falkon_homepage_client_images'];

	foreach($thumb_array as $image_id) {
		$image_attributes = wp_get_attachment_image_src( $image_id, 'full-size');
		echo '<div class="col-xs-4 col-sm-2 text-center">';
		echo '<img src="'.$image_attributes[0].'">';
		echo '</div>';
	}
?>
	</div>
</section>


<footer>
<div id="main-footer">
	<div class="container">
		<div class="col-xs-12 col-sm-8">
			<p>
<!--				--><?php //echo  '&copy; Copyright '. get_bloginfo() . ' '; echo date("Y") . ' | ' . do_shortcode("[company-contact-number link=true international=true]"); ?><!--<br>-->

				McDonald ICT Ltd a company registered in England and Wales with the company number <?php if($falkon_option['falkon_company_register_no']!='') echo nl2br($falkon_option['falkon_company_register_no']);?>
				and VAT GB <?php if($falkon_option['falkon_company_vat_no']!='') echo nl2br($falkon_option['falkon_company_vat_no']);?> whose registered office is <?php if($falkon_option['falkon_contact_address']!='') echo nl2br($falkon_option['falkon_contact_address']); ?>

			</p>

		</div>
        <div class="col-xs-12 col-sm-4">
		        <img src="<?php echo get_stylesheet_directory_uri();?>/images/logo-inverse.png" class="site-logo">
        </div>
<!--		--><?php
//		$social_links_header = $falkon_option['falkon_multicheckbox_inputs'];
//		if(count($social_links_header)>0){
//			foreach($social_links_header as $social_url => $value){
//				//				var_dump($value);
//				//var_dump($falkon_option[$social_url]);
//				if($value and $falkon_option[$social_url]!='') echo '<a href="'.esc_url($falkon_option[$social_url]).'" class="sociallink2" rel="nofollow" target="_blank"><i class="ss ss1 fa fa-lg fa-custom-'.$social_url.'"></i><i class="ss ss2 fa fa-lg fa-custom-'.$social_url.'"></i></a>';
//			}
//		}
//		?>
	</div>
</div>
</footer>


<script>
	jQuery(document).ready(function ($) {
		$("#clientlogo-carousel").owlCarousel({
//			autoPlay: 3000, //Set AutoPlay to 3 seconds
			items : 4,
			itemsDesktop : [1199,3],
			itemsDesktopSmall : [979,3],
			pagination : false,
			navigation:false,
			navigationText: [
				"<i class='fa fa-chevron-left fa-3x'></i>",
				"<i class='fa fa-chevron-right fa-3x'></i>"
			]
		});
		$("#clientwwd-carousel").owlCarousel({
			autoPlay: 3000, //Set AutoPlay to 3 seconds
			items : 3,
			itemsDesktop : [1199,3],
			itemsDesktop : [1199,3],
			itemsDesktopSmall : [979,3],
			pagination : false
		});
		$("#clientwork-carousel").owlCarousel({
			autoPlay: 3000, //Set AutoPlay to 3 seconds
			items : 4,
			itemsDesktop : [1199,3],
			itemsDesktopSmall : [979,3],
			pagination : false
		});
		$("#owl-childpage").owlCarousel({
			navigation : true,
			slideSpeed : 300,
			paginationSpeed : 500,
			singleItem:true,
			autoPlay : 4000
		});
		$("#owl-testimonials").owlCarousel({
			singleItem : true,
			pagination:true,
			transitionStyle : "fade",
			autoPlay: $("#owl-testimonials > div").length > 1 ? 5000 : false
		});

	});

</script>

<?php wp_footer(); ?>
</body>
</html>
