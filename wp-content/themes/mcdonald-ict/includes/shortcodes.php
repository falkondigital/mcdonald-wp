<?php
/**
 *
 */

add_shortcode("mic-get-trial-days","return_mic_get_trial_days");
function return_mic_get_trial_days($atts, $content = null) {
    global $falkon_option;
    $trial_weeks = $falkon_option['falkon_trial_weeks'];
    $trial_in_days = $trial_weeks*7;
    $out = '<span class="trial-period trial-in-days">'.$trial_in_days.'</span>';
    return $out;
}

add_shortcode("mic-get-trial-weeks","return_mic_get_trial_weeks");
function return_mic_get_trial_weeks($atts, $content = null) {
    global $falkon_option;
    $trial_weeks = $falkon_option['falkon_trial_weeks'];
    $out = '<span class="trial-period trial-in-weeks">'.$trial_weeks.'</span>';
    return $out;
}

add_shortcode( 'mic-trial-button', 'build_mic_trial_button' );
function build_mic_trial_button($atts, $content = null) {
    extract(shortcode_atts(array(
        'xclass'	=> '',
    ), $atts));
    $xclass = ($xclass == '') ? '' : ' '.$xclass;
    global $falkon_option;
    $trial_register_id = (int)$falkon_option['falkon_register_school_page_id'];
    $out = '<a href="'.get_permalink($trial_register_id).'" class="btn'.$xclass.'" title="'.esc_attr(wp_strip_all_tags(do_shortcode($content))).'">'.do_shortcode($content).'</a>';
    return $out;
}


/**
 * @param $atts
 * @param null $content
 * @return string
 */
function falkon_themes_site_contact_email( $atts, $content = null ) {
    $falkon_option = falkon_get_global_options();
    $out = '';
    if($falkon_option['falkon_contact_email']!=NULL and $falkon_option['falkon_contact_email']!=''){
        extract(shortcode_atts(array(
            'link'	=> false,
        ), $atts));
        if($link) $out = '<a href="mailto:'.antispambot($falkon_option['falkon_contact_email'],1).'" title="'.antispambot($falkon_option['falkon_contact_email'],0).'" rel="nofollow">'.antispambot($falkon_option['falkon_contact_email'],0).'</a>';
        else $out = $falkon_option['falkon_contact_email'];
    }
    return $out;

}
add_shortcode('company-contact-email', 'falkon_themes_site_contact_email');

function falkon_themes_site_contact_phone( $atts, $content = null ) {
    $falkon_option = falkon_get_global_options();
    $out = '';
    if($falkon_option['falkon_contact_number']!=NULL and $falkon_option['falkon_contact_number']!=''){
        extract(shortcode_atts(array(
            'link'	=> false,
            'international'   =>  false,
        ), $atts));
        $number_is = $falkon_option['falkon_contact_number'];
        $number_link = preg_replace('/\s+/', '', $falkon_option['falkon_contact_number']);
        if($international){
            $number_is = substr($falkon_option['falkon_contact_number'], 1);
            $number_is = $number_is;
            $number_link = preg_replace('/\s+/', '', $falkon_option['falkon_contact_number']);
            $number_link = "044".substr($number_link, 1);
        }
        if($link) $out = '<a href="tel:'.antispambot($number_link,1).'" title="'.$number_is.'" rel="nofollow" class="telephone-link">'.$number_is.'</a>';
        else $out = $number_is;
    }
    return $out;
}
add_shortcode('company-contact-number', 'falkon_themes_site_contact_phone');

function falkon_themes_site_contact_fax( $atts, $content = null ) {
    $falkon_option = falkon_get_global_options();
    $out = '';
    if($falkon_option['falkon_site_fax']!=NULL and $falkon_option['falkon_site_fax']!=''){
        $out = $falkon_option['falkon_site_fax'];
    }
    return $out;
}
add_shortcode('company-contact-fax', 'falkon_themes_site_contact_fax');

function falkon_themes_site_contact_address( $atts, $content = null ) {
    $falkon_option = falkon_get_global_options();
    $out = '';
    if($falkon_option['falkon_site_address']!=NULL and $falkon_option['falkon_site_address']!=''){
        $out = nl2br($falkon_option['falkon_site_address']);
    }
    return $out;
}
add_shortcode('company-contact-address', 'falkon_themes_site_contact_address');



function falkon_show_location_google_map( $atts, $content = null )
{
    extract(shortcode_atts(array(
        'url' => '',
    ), $atts));
    if($url=='')
        return;
//    if($title=='')
//        $title = __('Click To Expand Location','admedsol');


    $out = '';
    $out .= '<div class="google-maps">';
    $out .= '<iframe src="'.esc_url($url).'" width="850" height="370" frameborder="0" style="border:0"></iframe>';
//    $out .= '<p></p>';
//    $out .= '<a href="'.esc_url($url).'"'.($external?' rel="nofollow" target="_blank"':'').' title="'.esc_attr($title).'"><i class="fa fa-'.($external?'external-':'').'link"></i> '.$title.'</a>';
    $out .= '</div>';

    return $out;
}
add_shortcode('google-map-embed', 'falkon_show_location_google_map');


// Location
function falkon_maps( $atts ) {
	extract( shortcode_atts(
			array(
				'title' => '',
				'subtitle' => '',
				'map' => '',
				'address' => '',
				'phone' => '',
				'phone2' => '',
				'flip' => '',
				'email'     =>  '',
			), $atts )
	);

	$map_location = $map!=''?$map:'';

	if($map_location=='')
		$map_location='https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d9546.004995849298!2d-2.496406!3d53.262638!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487af8db5ca350b3%3A0x422c47b2f71f15e7!2sPriory+Rentals!5e0!3m2!1sen!2suk!4v1478620833556';
	$maps = '
   <div class="container-fluid location-sc-map ' .($flip==''?'':' box-flip'). '">
   <div class="container">
    <div class="location-box match-my-cols padd-bottom" data-equalizer="map" data-equalize-on="medium">
        <div class="col-xs-12 col-sm-6 location-content'.($flip==''?'':' box-flip').'" data-equalizer-watch="map">
            <div class="alignmentfl' .($flip==''?'':' box-flip').'">
            <h4>' . $title . '</h4>
            <p><strong>Address:</strong></p>
            <p>'.html_entity_decode($address).'</p>
            <p><i class="fa fa-phone-square red-hl"></i> <a href="tel:'.$phone.'">' . $phone . '</a></p>
            <p><i class="fa fa-phone-square red-hl"></i> <a href="tel:'.$phone2.'">' . $phone2 . '</a></p>
            <p><i class="fa fa-envelope-square red-hl"></i> <a href="mailto:'.antispambot($email).'"><span class="mailer-sp">' . antispambot($email) . '</span></a></p>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 location-map'.($flip==''?'':' box-flip').'">
            <div class="map-container">
                <iframe data-equalizer-watch="map" src="'.esc_url($map_location).'" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
    </div>
    </div>
    ';


	return $maps;
}
add_shortcode( 'maps', 'falkon_maps' );

// DISPLAY LOGO CAROUSEL FOR HOME
function home_logo_carousel() {
	ob_start();
	?>
	<div id="clientlogo-carousel" class="client-work">
		<?php
		global $artiste_gallery_mb;
		$artiste_gallery = $artiste_gallery_mb->the_meta();

		$thumb_array = $artiste_gallery['artiste_gal'];

		if ( is_array( $thumb_array ) ) {
			foreach ( $thumb_array as $image_id ) {

				$image = wp_get_attachment_image_src( $image_id, 'large', FALSE );

				echo '<div><img src="' . $image[0] . '" alt="" class="logo-thumb" /></div>';
			}
		}
		?>
	</div>
	<?php
	return ob_get_clean();
}
add_shortcode('hp_carousel', 'home_logo_carousel');


// TESTIMONIALS
function add_testimonials ( $atts, $content = null ) {
	ob_start();
	global $testimonial_control_mb;
	$tml_control = $testimonial_control_mb->the_meta();
	if (isset( $tml_control['testimonial_select'])) {
		echo '<section id="testimonial-block">';
		echo '<div class="quote-block">';
		echo '<div id="owl-testimonials" class="owl-carousel">';
		$testimonial_array = $tml_control['testimonial_select'];
		if ( is_array( $testimonial_array ) ) {
			foreach ( $testimonial_array as $testimonial_slide ) {
				global $testimonial_mb;
				$testimonial_meta = $testimonial_mb->the_meta($testimonial_slide);
//				var_dump($testimonial_meta);
				echo '<div>';
				echo '<strong>' . $testimonial_meta['description'] . '</strong>';
//				echo '<span class="name-title">' . $testimonial_meta['name'] . '</span><br/>';
//				echo '<span class="job-title">' . $testimonial_meta['position'].', '.$testimonial_meta['company'].'</span>';
				echo '</div>';
			}
		}
		echo '</div>';
		echo '</div>';
		echo '</section>';
		return ob_get_clean();
	}
}
add_shortcode('show-testimonials', 'add_testimonials');


// BLUE BLOCK - SERVICES
function services_blblock() {
	ob_start();
	?>
	<?php
		global $services_meta;
		$serv_meta = $services_meta->the_meta();
	?>
	    </div>
	  </div>
	</div>
	<section id="blue-block">
       <div class="container-fluid np">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
	          <div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-3 col-lg-8 vert-bp">
		        <?php echo wpautop(do_shortcode($serv_meta['leftserv_editor']))  ?>
	          </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 np">
 	            <img src="<?php echo $serv_meta['imgserv_right']; ?>" class="info-imgs">
          </div>
       </div>
    </section>
    <div class="container">
	<?php

		?>
	<?php
	return ob_get_clean();
}
add_shortcode('blue_block', 'services_blblock');