<?php
/**
 * Single post template for CPT 'our-work'
 */
?>
<?php

//	global $banner_mb;
//	$banner_meta = $banner_mb->the_meta();
	global $our_work_mb;
	$our_work_meta = $our_work_mb->the_meta();

	// Falkon Options
	global $falkon_option;
?>
<?php get_template_part('includes/header'); ?>

<?php //var_dump($banner_meta);
//?>

<div id="owl-childpage" class="owl-carousel">
	<?php
	if (isset( $banner_meta['banner_slides'])) {

		$banner_array = $banner_meta['banner_slides'];
		if ( is_array( $banner_array ) ) {
			foreach ( $banner_array as $banner_slide ) {
				//var_dump($banner_slide);

				echo '<div>';
				echo '<img src="' . $banner_slide['imgurl'] . '" class="ow-childpage-slide"/>';
				echo '<div class="wwd-childpage-overlay hidden-xs">';
				echo '<h3>' . $banner_slide['title_line'] . '</h3>';
				echo '<span class="wwd-childpage-tag">' . $banner_slide['desc'] . '<span>';
				echo '</div>';
				echo '</div>';
			}

		}

	}
	?>
</div>


	<div class="container">
		<section id="ourwork-content">
		   <div id="content" class="col-xs-12 col-sm-offset-1 col-sm-10 col-md-offset-1 col-md-10 text-center">
			   <?php get_template_part('includes/loops/content', 'page'); ?>
		   </div>
		</section>


	</div>
<!---->
<!--<section id="related-projects">-->
<!--	--><?php //include(locate_template('includes/blocks/related-work.php'));?>
<!--</section>-->
<!---->




<?php get_template_part('includes/footer'); ?>
